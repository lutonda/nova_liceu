using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Interface.Generic;
using Infra.Config;
using Microsoft.EntityFrameworkCore;

namespace Infra.Repository.Generic
{
    public class RepositoryGeneric<T> : InterfaceGeneric<T>, IDisposable  where T : class
    {
        private DbContextOptionsBuilder<ContextBase> _OptionsBuilder;

        public RepositoryGeneric()
        {
            _OptionsBuilder = new DbContextOptionsBuilder<ContextBase>();
        }

        ~RepositoryGeneric()
        {
            Dispose(false);
        }

        public void Add(T Entity)
        {
            using (var db = new ContextBase(_OptionsBuilder.Options))
            {
                db.Add(Entity);
                db.SaveChanges();
            }
        }

        public void Update(T Entity)
        {
            using (var db = new ContextBase(_OptionsBuilder.Options))
            {
                db.Update(Entity);
                db.SaveChanges();
            }
        }

        public void Delete(Guid Id)
        {
            using (var db = new ContextBase(_OptionsBuilder.Options))
            {
                var obj = db.Set<T>().Find(Id);
                db.Remove(obj);
                db.SaveChanges();
            }
        }

        public List<T> List()
        {
            using (var db = new ContextBase(_OptionsBuilder.Options))
            {
                
                return db.Set<T>().ToList();
            }
        }

        public T Show(Guid id)
        {
            using (var db = new ContextBase(_OptionsBuilder.Options))
            {
                var obj = db.Set<T>().Find(id);
                return obj;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool status)
        {
            if (!status) return;
        }
    }
}