using Domain.Entity;
using Domain.Entity.Users;
using Domain.Interface;
using Infra.Repository.Generic;

namespace Infra.Repository
{
    public class RepositoryUser : RepositoryGeneric<User>, IUser
    {
        
    }
}