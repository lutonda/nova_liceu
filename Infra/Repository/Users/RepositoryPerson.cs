using Domain.Entity.Users;
using Domain.Interface.Users;
using Infra.Repository.Generic;

namespace Infra.Repository.Users
{
    public class RepositoryPerson : RepositoryGeneric<Person>, IPerson
    {
        
    }
}