using Domain.Entity.Users;
using Domain.Interface.Users;
using Infra.Repository.Generic;

namespace Infra.Repository.Users
{
    public class RepositoryIdCardType : RepositoryGeneric<IdCardType>, IIdCardType
    {
        
    }
}