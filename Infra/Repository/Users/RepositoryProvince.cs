using Domain.Entity.Users;
using Domain.Interface.Users;
using Infra.Repository.Generic;

namespace Infra.Repository.Users
{
    public class RepositoryProvince : RepositoryGeneric<Province>, IProvince
    {
        
    }
}