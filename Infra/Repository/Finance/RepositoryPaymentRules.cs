﻿using Domain.Entity.Finance;
using Domain.Interface.Finance;
using Infra.Repository.Generic;

namespace Infra.Repository.Finance
{
    public class RepositoryPaymentRules : RepositoryGeneric<PaymentRules>, IPaymentRules
    {
    }
}
