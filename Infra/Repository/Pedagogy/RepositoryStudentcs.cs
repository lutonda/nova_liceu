using Domain.Entity;
using Domain.Entity.Pedagogy;
using Domain.Entity.Users;
using Domain.Interface;
using Domain.Interface.Pedagogy;
using Infra.Repository.Generic;

namespace Infra.Repository.Pedagogy
{
    public class RepositoryStudent : RepositoryGeneric<Student>, IStudent
    {
        
    }
}