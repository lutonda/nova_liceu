﻿using Domain.Entity.Pedagogy;
using Domain.Interface.Pedagogy;
using Infra.Repository.Generic;

namespace Infra.Repository.Pedagogy
{
    public class RepositoryAlocatedSubject : RepositoryGeneric<AlocatedSubject>, IAlocatedSubject
    {
    }
}
