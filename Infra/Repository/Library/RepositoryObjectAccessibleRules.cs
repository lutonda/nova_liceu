﻿using Domain.Entity.Library;
using Domain.Interface.Library;
using Infra.Repository.Generic;

namespace Infra.Repository.Library
{
    public class RepositoryObjectAccessibleRules : RepositoryGeneric<ObjectAccessibleRules>, IObjectAccessibleRules
    {
    }
}
