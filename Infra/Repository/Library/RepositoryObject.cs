﻿using Domain.Entity.Library;
using Domain.Interface.Library;
using Infra.Repository.Generic;

namespace Infra.Repository.Library
{
    public class RepositoryObject : RepositoryGeneric<Object>, IObject
    {
    }
}
