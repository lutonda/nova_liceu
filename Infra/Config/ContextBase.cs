using System.IO;
using Domain.Entity;
using Domain.Entity.Pedagogy;
using Domain.Entity.Finance;
using Domain.Entity.Users;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Domain.Entity.Library;

namespace Infra.Config
{
    public class ContextBase : DbContext
    {

        public IConfigurationRoot Configuration { get; set; }

        public ContextBase(DbContextOptions<ContextBase> option) : base(option)
        {
            Database.EnsureCreated();
        }

        public ContextBase()
        {
            //throw  new System.NotImplementedException();
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Person> Persons { get; set; }
        public DbSet<Township> Townships { get; set; }
        public DbSet<Sex> Sexs { get; set; }
        public DbSet<Province> Provinces { get; set; }
        public DbSet<IdCardType> IdCardTypes { get; set; }
        public DbSet<IdCard> IdCards { get; set; }
        public DbSet<Health> Healths { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<ContactType> ContactTypes { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<BloodGroup> BloodGroups { get; set; }
        public DbSet<Address> Adresses { get; set; }
        public DbSet<AcademiClass> AcademiClasses { get; set; }
        public DbSet<Grade> Grades { get; set; }
        public DbSet<Period> Periods { get; set; }
        public DbSet<Professor> Professors { get; set; }
        public DbSet<RegistrationClass> RegistrationClasses { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<Subject> Subjects { get; set; }

        //Pedagogy
        public DbSet<AlocatedSubject> AlocatedSubjects { get; set; }
        public DbSet<Score> Scores { get; set; }
        public DbSet<StrudentSponsor> StrudentSponsors { get; set; }
        public DbSet<TimeSheet> TimeSheets { get; set; }
        public DbSet<TimeTable> TimeTables { get; set; }


        //User


        //Finance
        public DbSet<Payment> Payments { get; set; }
        public DbSet<PaymentRules> PaymentRuless { get; set; }
        public DbSet<PaymentSlip> PaymentSlips { get; set; }


        //Library
        public DbSet<Object> Objects { get; set; }
        public DbSet<ObjectAccessibleRules> ObjectAccessibleRulesses { get; set; }
        public DbSet<ObjectType> ObjectTypes { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionBuilder)
        {
            if (!optionBuilder.IsConfigured)
                optionBuilder.UseMySql(RetornaUrlConection());
        }

        public string RetornaUrlConection()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");
            Configuration = builder.Build();

            string conexao = Configuration.GetConnectionString("DefaultConnection");
            return conexao;
        }
    }
}
