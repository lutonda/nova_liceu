using System.Linq;
using Domain.Entity.Pedagogy;
using Domain.Entity.Users;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Infra.Config
{
    public class InitValuesBase
    {
        private DbContextOptionsBuilder<ContextBase> _OptionsBuilder;

          
        public InitValuesBase()
        {  
            
            _OptionsBuilder = new DbContextOptionsBuilder<ContextBase>();
            var context = new ContextBase(_OptionsBuilder.Options);
            
            if (!context.Periods.Any())
            {
                context.Periods.Add(new Period {Name = "Manha", Code = "M"});
                context.Periods.Add(new Period {Name = "Tarde", Code = "T"});
                context.Periods.Add(new Period {Name = "Noturno", Code = "N"});
                context.SaveChanges();
            }

            if (!context.Rooms.Any())
            {
                context.Rooms.Add(new Room {Name = "Naians", Number = "A09", Size = 25});
                context.Rooms.Add(new Room {Name = "Doians", Number = "A08", Size = 35});
                context.Rooms.Add(new Room {Name = "Vonians", Number = "A07", Size = 15});
                context.SaveChanges();
            }

            if (!context.Grades.Any())
            {
                context.Grades.Add(new Grade {Number = 9});
                context.Grades.Add(new Grade {Number = 10});
                context.Grades.Add(new Grade {Number = 11});
                context.Grades.Add(new Grade {Number = 12});
                context.SaveChanges();
            }
             if (!context.Sexs.Any())
            {
                context.Sexs.Add(new Sex {Name = "Masculino", Code = "M"});
                context.Sexs.Add(new Sex {Name = "Feminino", Code = "F"});
                context.SaveChanges();
            }

            Country country = new Country {Name = "Angola"};
            if (!context.Countries.Any())
            {     
                context.Countries.Add(country);
                context.SaveChanges();
            }

             var provinces = new string[]
            {
                "BENGO", "BENGUELA", "BIÉ", "CABINDA", "CUNENE", "HUAMBO", "HUILA", "KUANDO KUBANGO", "KWANZA NORTE",
                "KWANZA SUL", "LUANDA", "LUNDA NORTE", "LUNDA SUL", "MALANGE", "MOXICO", "NAMIBE", "UIGE", "ZAIRE"
            };
             if (!context.Provinces.Any())
             {
                 foreach (var province in provinces)
                 {
                     context.Provinces.Add(new Province {Name = province, Country = country});
                 }
                 context.SaveChanges();
             }

            var subjects = new string[]
            {
                "Lingua Portuguesa", "Lingua Inglesa", "Lingua Francesa", "Matemática", "Física", "Quimica", "Biologia",
                "História", "Geografia", "Geologia", "Informática"
            };
            if (!context.Subjects.Any())
            {
                 foreach (var subject in subjects)
                 {
                     context.Subjects.Add(new Subject {Name = subject});
                 }
                 context.SaveChanges();
            }

            var townships = new string[]
            {
                "Cacuaco", "Belas", "Cazenga", "Ícolo e Bengo", "Luanda", "Quissama e Viana", "Talatona",
                "Kilamba-Kiaxi"
            };
            Township t;
            if (!context.Townships.Any())
            {
                var luanda = context.Provinces.SingleOrDefault(p => p.Name == "Luanda");
                foreach (var township in townships)
                {
                    t=new Township{Name = township, Province = luanda};
                    context.Townships.Add(t);
                }
                context.SaveChanges();
            }
            

            if (!context.ContactTypes.Any())
            {
                context.ContactTypes.Add(new ContactType {Name = "Email", Code = "E"});
                context.ContactTypes.Add(new ContactType {Name = "Telefone", Code = "T"});
                context.SaveChanges();
            }
            if (!context.IdCardTypes.Any())
            {
                context.IdCardTypes.Add(new IdCardType {Name = "Bilhete de Identidade", Code = "BI"});
                context.IdCardTypes.Add(new IdCardType{Name = "Cedula", Code = "CD"});
                context.IdCardTypes.Add(new IdCardType{Name = "Passporte", Code = "PS"});
                context.SaveChanges();
            }

            if (!context.Professors.Any())
            {
                var person = new Person {FirstName = "Nelson", LastName = "Dinis", Code = 9999};
                var p= context.Persons.Add(person);
                context.SaveChanges();
                
                context.Professors.Add(new Professor {Person = person});
                
                context.SaveChanges();
            }

        }
    }
}