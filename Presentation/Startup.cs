﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Application.App;
using Application.App.Pedagogy;
using Application.App.Users;
using Application.Interface;
using Application.Interface.Pedagogy;
using Application.Interface.Users;
using Domain.Entity.Pedagogy;
using Domain.Interface;
using Domain.Interface.Generic;
using Domain.Interface.Pedagogy;
using Domain.Interface.Users;
using Infra.Repository;
using Infra.Repository.Generic;
using Infra.Repository.Pedagogy;
using Infra.Repository.Users;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace Presentation
{
    public class Startup
    {
       public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            if (env.IsDevelopment())
            {
                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                builder.AddApplicationInsightsSettings(developerMode: true);
            }
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer=true,
                    ValidateAudience=true,
                    ValidateIssuerSigningKey=true,
                    ValidIssuer="mysite.com",
                    ValidAudience="mysite.com",
                    IssuerSigningKey=  new SymmetricSecurityKey(Encoding.UTF8.GetBytes("ahbasshfbsahjfbshajbfhjasbfashjbfsajhfvashjfashfbsahfbsahfksdjf"))
                   
                };
            });
            services.AddCors(options =>
            {
                options.AddPolicy("AllowSpecificOrigin",
                    builder => builder
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .WithOrigins("http://localhost:4200"));
            });
            // Add framework services.
            services.AddApplicationInsightsTelemetry(Configuration);

            services.AddMvc()
                .AddJsonOptions(
                    options => options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
                );

            services.AddSingleton(typeof(InterfaceGeneric<>), typeof(RepositoryGeneric<>));

            services.AddSingleton<IUser, RepositoryUser>();
            services.AddSingleton<IAppUser, AppUser>();

            services.AddSingleton<IAcademiClass, RepositoryAcademiClass>();
            services.AddSingleton<IAppAcademiClass, AppAcademiClass>();
            
            services.AddSingleton<IPeriod, RepositoryPeriod>();
            services.AddSingleton<IAppPeriod, AppPeriod>();
            
            services.AddSingleton<IGrade, RepositoryGrade>();
            services.AddSingleton<IAppGrade, AppGrade>();
            
            services.AddSingleton<IAppRoom, AppRoom>();
            services.AddSingleton<IRoom, RepositoryRoom>();

            services.AddSingleton<IAppProfessor, AppProfessor>();
            services.AddSingleton<IProfessor, RepositoryProfessor>();

            services.AddSingleton<IAppStudent, AppStudent>();
            services.AddSingleton<IStudent, RepositoryStudent>();

            services.AddSingleton<IAppPerson, AppPerson>();
            services.AddSingleton<IPerson, RepositoryPerson>();

            services.AddSingleton<IAppSex, AppSex>();
            services.AddSingleton<ISex, RepositorySex>();

            services.AddSingleton<IAppRegistrationClass, AppRegistrationClass>();
            services.AddSingleton<IRegistrationClass, RepositoryRegistrationClass>();

            services.AddSingleton<IAppTownship, AppTownship>();
            services.AddSingleton<ITownship, RepositoryTownship>();

            services.AddSingleton<IAppIdCardType, AppIdCardType>();
            services.AddSingleton<IIdCardType, RepositoryIdCardType>();

            services.AddSingleton<IAppContactType, AppContactType>();
            services.AddSingleton<IContactType, RepositoryContactType>();

            services.AddSingleton<IAppProvince, AppProvince>();
            services.AddSingleton<IProvince, RepositoryProvince>();

            services.AddSingleton<IAppSubject, AppSubject>();
            services.AddSingleton<ISubject, RepositorySubject>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseAuthentication();
            app.UseCors("AllowSpecificOrigin");
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseApplicationInsightsRequestTelemetry();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                //app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            
            app.UseApplicationInsightsExceptionTelemetry();

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Produto}/{action=Index}/{id?}/{data?}");
            });
        }
    }
}