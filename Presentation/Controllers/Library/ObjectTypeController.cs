﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Interface.Library;
using Domain.Entity.Library;
using Infra.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Presentation.Controllers.Library
{
    [Authorize]
    [Route("Api/Library/[Controller]")]
    public class ObjectTypeController : ControllerBase
    {
        private readonly IAppObjectType _iAppObjectType;

        public ObjectTypeController(IAppObjectType iAppObjectType)
        {
            _iAppObjectType = iAppObjectType;
        }

        // GET api/library/objectType
        [HttpGet]
        [Produces("application/Json")]
        public IActionResult Get()
        {
            var final = new List<ObjectType>();
            var context = new ContextBase();
            var objectTypes = context.ObjectTypes.ToList();

            foreach (var objectType in objectTypes)
            {
                final.Add(objectType);
            }

            return Ok(final);
        }

        // GET api/library/objecttype
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var context = new ContextBase();
            var objectType = context.ObjectTypes.SingleOrDefault(x => x.Id == id);

            return Ok(objectType);
        }

        // Post api/library/objecttype
        [HttpPost]
        [Produces("application/json")]
        [HttpGet("{id}")]
        [HttpGet("{data}")]
        public IActionResult Post([FromBody] ObjectType objectType)
        {
            
            _iAppObjectType.Update(objectType);

            //_iRegistrationClass.Add(registrationClass);

            return Ok(objectType);
        }

        // Post api/users
        [HttpGet]
        [Produces("application/json")]
        [Route("new")]
        public IActionResult New()
        {

            return Ok(null);
        }
    }
}
