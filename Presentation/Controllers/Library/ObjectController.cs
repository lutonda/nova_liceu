﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Interface.Library;
using Domain.Entity.Library;
using Infra.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Presentation.Controllers.Library
{
    [Authorize]
    [Route("Api/pedagogy/[Controller]")]
    public class LibraryController : ControllerBase
    {
        private readonly IAppObject _IAppObject;
        private readonly IAppObjectType _IAppObjectType;
        private readonly IAppObjectAccessibleRules _IAppObjectAccessibleRules;


        public LibraryController(
            IAppObject IAppObject,
            IAppObjectType IAppObjectType,
            IAppObjectAccessibleRules IAppObjectAccessibleRules
        )
        {
            _IAppObject = IAppObject;
            _IAppObjectType = IAppObjectType;
            _IAppObjectAccessibleRules = IAppObjectAccessibleRules;
        }

        // GET api/library/object
        [HttpGet]
        [Produces("application/Json")]
        public IActionResult Get()
        {

            var final = new List<Domain.Entity.Library.Object>();
            var context = new ContextBase();
            var objects = context.Objects.Include(p => p.type).Include(p => p.rules).ToList();

            return Ok(final);
        }

        // GET api/library/object
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var context = new ContextBase();
            var objects = context.Objects.Include(p => p.type).Include(p => p.rules)
                .SingleOrDefault(x => x.Id == id);

            return Ok(objects);
        }

        // Post api/library/object
        [HttpPost]
        [Produces("application/json")]
        public IActionResult Post([FromBody] Domain.Entity.Library.Object objects)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            objects.type = null;
            objects.rules = null;
           
            _IAppObject.Update(objects);

            return Ok(objects);
        }

        // Post api/users
        [HttpGet]
        [Produces("application/json")]
        [Route("new")]
        public IActionResult New()
        {
            var final = new
            {
                Rules = _IAppObjectAccessibleRules.List(),
                Type = _IAppObjectType.List(),
                
                Years = new List<string> { DateTime.Now.ToString("yyyy"), DateTime.Now.AddYears(1).ToString("yyyy") }
            };

            return Ok(final);
        }
    }
}
