﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Interface.Library;
using Domain.Entity.Library;
using Infra.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Presentation.Controllers.Library
{
    [Authorize]
    [Route("Api/pedagogy/[Controller]")]
    public class ObjectAccessibleRulesController : ControllerBase
    {
        private readonly IAppObjectAccessibleRules _IAppObjectAccessibleRules;

        public ObjectAccessibleRulesController(IAppObjectAccessibleRules IAppObjectAccessibleRules)
        {
            _IAppObjectAccessibleRules = IAppObjectAccessibleRules;
        }

        // GET api/library/objectaccessiblerules
        [HttpGet]
        [Produces("application/Json")]
        public IActionResult Get()
        {

            var final = new List<ObjectAccessibleRules>();
            var context = new ContextBase();
            var objectAccessibleRules = context.ObjectAccessibleRulesses.ToList();

            foreach (var objectAcessibleRule in objectAccessibleRules)
            {
                final.Add(objectAcessibleRule);
            }

            return Ok(final);
        }

        // GET api/library/objectaccessiblerules
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var context = new ContextBase();
            var objectAccessibleRules = context.ObjectAccessibleRulesses.SingleOrDefault(x => x.Id == id);
            
            return Ok(objectAccessibleRules);
        }

        // Post api/library/objectaccessiblerules
        [HttpPost]
        [Produces("application/json")]
        public IActionResult Post([FromBody] ObjectAccessibleRules objectAccessibleRules)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _IAppObjectAccessibleRules.Update(objectAccessibleRules);

            return Ok(objectAccessibleRules);
        }

        // Post api/users
        [HttpGet]
        [Produces("application/json")]
        [Route("new")]
        public IActionResult New()
        {
            var final = new
            {
                Years = new List<string> { DateTime.Now.ToString("yyyy"), DateTime.Now.AddYears(1).ToString("yyyy") }
            };

            return Ok(final);
        }
    }
}
