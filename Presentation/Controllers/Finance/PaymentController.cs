﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Interface.Finance;
using Domain.Entity.Finance;
using Infra.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Presentation.Controllers.Finance
{
    [Authorize]
    [Route("Api/Finance/[Controller]")]
    public class PaymentController : ControllerBase
    {
        private readonly IAppPayment _IAppPayment;

        public PaymentController(IAppPayment iAppPayment)
        {
            _IAppPayment = iAppPayment;
        }

        // GET api/finance/payment
        [HttpGet]
        [Produces("application/Json")]
        public IActionResult Get()
        {

            var final = new List<Payment>();
            var context = new ContextBase();
            var payments = context.Payments.ToList();

            foreach (var payment in payments)
            {
                final.Add(payment);
            }

            return Ok(final);
        }

        // GET api/finance/payment
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var context = new ContextBase();
            var payment = context.Payments.SingleOrDefault(x => x.Id == id);

            return Ok(payment);
        }

        // Post api/finance/payment
        [HttpPost]
        [Produces("application/json")]
        public IActionResult Post([FromBody] Payment payment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _IAppPayment.Update(payment);

            return Ok(payment);
        }

        // Post api/users
        [HttpGet]
        [Produces("application/json")]
        [Route("new")]
        public IActionResult New()
        {
            var final = new
            {
                Years = new List<string> { DateTime.Now.ToString("yyyy"), DateTime.Now.AddYears(1).ToString("yyyy") }
            };

            return Ok(final);
        }
    }
}
