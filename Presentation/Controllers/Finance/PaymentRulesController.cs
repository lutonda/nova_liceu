﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Interface.Finance;
using Application.Interface.Pedagogy;
using Domain.Entity.Finance;
using Infra.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Presentation.Controllers.Finance
{
    [Authorize]
    [Route("Api/Finance/[Controller]")]
    public class PaymentRulesController : ControllerBase
    {
        private readonly IAppPaymentRules _IAppPaymentRules;
        private readonly IAppAcademiClass _IAppAcademiClass;
        
        public PaymentRulesController(
            IAppPaymentRules IAppPaymentRules,
            IAppAcademiClass IAppAcademiClass
        )
        {
            _IAppPaymentRules = IAppPaymentRules;
            _IAppAcademiClass = IAppAcademiClass;
        }

        // GET api/finance/paymentrules
        [HttpGet]
        [Produces("application/Json")]
        public IActionResult Get()
        {

            var final = new List<PaymentRules>();
            var context = new ContextBase();
            var paymentRules = context.PaymentRuless.Include(p => p.Class).ToList();

            foreach (var paymentRule in paymentRules)
            {
                final.Add(paymentRule);
            }

            return Ok(final);
        }

        // GET api/finance/paymentrules
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var context = new ContextBase();
            var paymentRules = context.PaymentRuless.Include(p => p.Class).SingleOrDefault(x => x.Id == id);

            return Ok(paymentRules);
        }

        // Post api/finance/paymentrules
        [HttpPost]
        [Produces("application/json")]
        public IActionResult Post([FromBody] PaymentRules paymentRules)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            paymentRules.Class = null;
            _IAppPaymentRules.Update(paymentRules);

            return Ok(paymentRules);
        }

        // Post api/users
        [HttpGet]
        [Produces("application/json")]
        [Route("new")]
        public IActionResult New()
        {
            var final = new
            {
                Classes = _IAppAcademiClass.List(),
                Years = new List<string> { DateTime.Now.ToString("yyyy"), DateTime.Now.AddYears(1).ToString("yyyy") }
            };

            return Ok(final);
        }
    }
}
