﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Interface.Finance;
using Application.Interface.Pedagogy;
using Domain.Entity.Finance;
using Infra.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
namespace Presentation.Controllers.Finance
{
    [Authorize]
    [Route("Api/Finance/[Controller]")]
    public class PaymentSlipController : ControllerBase
    {
        private readonly IAppPaymentSlip _iAppPaymentSlip;

        private readonly IAppPayment _iAppPayment;

        private readonly IAppRegistrationClass _iRegistrationClass;

        public PaymentSlipController(IAppPaymentSlip iAppPaymentSlip, IAppPayment iAppPayment, IAppRegistrationClass iRegistrationClass
        )
        {
            _iAppPaymentSlip = iAppPaymentSlip;
            _iAppPayment = iAppPayment;
            _iRegistrationClass = iRegistrationClass;
        }

        // GET api/Finance/PaymentSlip
        [HttpGet]
        [Produces("application/Json")]
        public IActionResult Get()
        {
            var final = new List<PaymentSlip>();
            var context = new ContextBase();
            var academicClasses = context.PaymentSlips.Include(p => p.registration).Include(p => p.payment).ToList();

            foreach (var academicClass in academicClasses)
            {
                final.Add(academicClass);
            }

            return Ok(final);
        }

        // GET api/finance/paymentSlip
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var context = new ContextBase();
            var paymentSlip = context.PaymentSlips.Include(p => p.registration).Include(p => p.payment)
                .SingleOrDefault(x => x.Id == id);

            return Ok(paymentSlip);
        }

        // Post api/finance/paymentslip
        [HttpPost]
        [Produces("application/json")]
        [HttpGet("{id}")]
        [HttpGet("{data}")]
        public IActionResult Post([FromBody] PaymentSlip paymentSlip)
        {


            paymentSlip.registration = null;
            paymentSlip.payment = null;


            _iAppPaymentSlip.Update(paymentSlip);

            //_iRegistrationClass.Add(registrationClass);

            return Ok(paymentSlip);
        }

        // Post api/users
        [HttpGet]
        [Produces("application/json")]
        [Route("new")]
        public IActionResult New()
        {

            return Ok(null);
        }
    }
}
