﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Interface.Users;
using Domain.Entity.Users;
using Infra.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Presentation.Controllers.Users
{
    [Authorize]
    [Route("Api/Users/[Controller]")]
    public class IdCardController : ControllerBase
    {
        private readonly IAppIdCard _IAppIdCard;
        private readonly IAppIdCardType _IAppIdCardType;
        public IdCardController(IAppIdCard iAppHealth, IAppIdCardType iAppIdCardType)
        {
            _IAppIdCard = iAppHealth;
            _IAppIdCardType = iAppIdCardType;
        }

        // GET api/Users/IdCard
        [HttpGet]
        [Produces("application/Json")]
        public IActionResult Get()
        {

            var final = new List<IdCard>();
            var context = new ContextBase();
            var idCards = context.IdCards.Include(p => p.IdCardType).ToList();

            foreach (var idCard in idCards)
            {
                final.Add(idCard);
            }

            return Ok(final);
        }

        // GET api/Users/IdCard
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var context = new ContextBase();
            var idCard = context.IdCards.Include(p => p.IdCardType).SingleOrDefault(x => x.Id == id);

            return Ok(idCard);
        }

        // Post api/users/IdCard
        [HttpPost]
        [Produces("application/json")]
        public IActionResult Post([FromBody] IdCard idCard)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _IAppIdCard.Update(idCard);

            return Ok(idCard);
        }

        // Post api/users
        [HttpGet]
        [Produces("application/json")]
        [Route("new")]
        public IActionResult New()
        {
            var final = new
            {
                IdCardTypes = _IAppIdCardType,
                Years = new List<string> { DateTime.Now.ToString("yyyy"), DateTime.Now.AddYears(1).ToString("yyyy") }
            };

            return Ok(final);
        }
    }
}
