﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Interface.Users;
using Domain.Entity.Users;
using Infra.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Presentation.Controllers.Users
{
    [Authorize]
    [Route("Api/Users/[Controller]")]
    public class SexController : ControllerBase
    {
        private readonly IAppSex _IAppSex;
        public SexController(IAppSex iAppSex)
        {
            _IAppSex = iAppSex;
        }

        // GET api/Users/Sex
        [HttpGet]
        [Produces("application/Json")]
        public IActionResult Get()
        {

            var final = new List<Sex>();
            var context = new ContextBase();
            var sexs = context.Sexs.ToList();

            foreach (var sex in sexs)
            {
                final.Add(sex);
            }

            return Ok(final);
        }

        // GET api/Users/Sex
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var context = new ContextBase();
            var sex = context.Sexs.SingleOrDefault(x => x.Id == id);

            return Ok(sex);
        }

        // Post api/users/Sex
        [HttpPost]
        [Produces("application/json")]
        public IActionResult Post([FromBody] Sex sex)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _IAppSex.Update(sex);

            return Ok(sex);
        }

        // Post api/users
        [HttpGet]
        [Produces("application/json")]
        [Route("new")]
        public IActionResult New()
        {
            var final = new
            {
                Years = new List<string> { DateTime.Now.ToString("yyyy"), DateTime.Now.AddYears(1).ToString("yyyy") }
            };

            return Ok(final);
        }
    }
}
