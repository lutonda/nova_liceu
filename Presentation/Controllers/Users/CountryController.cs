﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Interface.Users;
using Domain.Entity.Users;
using Infra.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Presentation.Controllers.Users
{
    [Authorize]
    [Route("Api/Users/[Controller]")]
    public class CountryController : ControllerBase
    {
        private readonly IAppCountry _IAppCountry;
        public CountryController(IAppCountry iAppCountry)
        {
            _IAppCountry = iAppCountry;
        }

        // GET api/Users/Country
        [HttpGet]
        [Produces("application/Json")]
        public IActionResult Get()
        {

            var final = new List<Country>();
            var context = new ContextBase();
            var countries = context.Countries.ToList();

            foreach (var country in countries)
            {
                final.Add(country);
            }

            return Ok(final);
        }

        // GET api/Users/Country
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var context = new ContextBase();
            var country = context.Countries.SingleOrDefault(x => x.Id == id);

            return Ok(country);
        }

        // Post api/users/Country
        [HttpPost]
        [Produces("application/json")]
        public IActionResult Post([FromBody] Country country)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _IAppCountry.Update(country);

            return Ok(country);
        }

        // Post api/users
        [HttpGet]
        [Produces("application/json")]
        [Route("new")]
        public IActionResult New()
        {
            var final = new
            {
                Years = new List<string> { DateTime.Now.ToString("yyyy"), DateTime.Now.AddYears(1).ToString("yyyy") }
            };

            return Ok(final);
        }
    }
}
