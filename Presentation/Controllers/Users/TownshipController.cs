﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Interface.Users;
using Domain.Entity.Users;
using Infra.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Presentation.Controllers.Users
{
    [Authorize]
    [Route("Api/Users/[Controller]")]
    public class TownshipController : ControllerBase
    {
        private readonly IAppTownship _IAppTownship;
        private readonly IAppProvince _IAppProvince;
        public TownshipController(IAppTownship iAppTownship, IAppProvince iAppProvince)
        {
            _IAppTownship = iAppTownship;
            _IAppProvince = iAppProvince;
        }

        // GET api/Users/Township
        [HttpGet]
        [Produces("application/Json")]
        public IActionResult Get()
        {

            var final = new List<Township>();
            var context = new ContextBase();
            var townships = context.Townships.Include(p => p.Province).ToList();

            foreach (var township in townships)
            {
                final.Add(township);
            }

            return Ok(final);
        }

        // GET api/Users/Township
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var context = new ContextBase();
            var township = context.Townships.Include(p => p.Province).SingleOrDefault(x => x.Id == id);

            return Ok(township);
        }

        // Post api/users/Township
        [HttpPost]
        [Produces("application/json")]
        public IActionResult Post([FromBody] Township township)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _IAppTownship.Update(township);

            return Ok(township);
        }

        // Post api/users
        [HttpGet]
        [Produces("application/json")]
        [Route("new")]
        public IActionResult New()
        {
            var final = new
            {
                Provinces = _IAppProvince,
                Years = new List<string> { DateTime.Now.ToString("yyyy"), DateTime.Now.AddYears(1).ToString("yyyy") }
            };

            return Ok(final);
        }
    }
}
