﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Interface.Users;
using Domain.Entity.Users;
using Infra.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Presentation.Controllers.Users
{
    [Authorize]
    [Route("Api/Users/[Controller]")]
    public class IdCardTypeController : ControllerBase
    {
        private readonly IAppIdCardType _IAppIdCardType;
        public IdCardTypeController(IAppIdCardType iAppIdCardType)
        {
            _IAppIdCardType = iAppIdCardType;
        }

        // GET api/Users/IdCardType
        [HttpGet]
        [Produces("application/Json")]
        public IActionResult Get()
        {

            var final = new List<IdCardType>();
            var context = new ContextBase();
            var idCardTypes = context.IdCardTypes.ToList();

            foreach (var idCardType in idCardTypes)
            {
                final.Add(idCardType);
            }

            return Ok(final);
        }

        // GET api/Users/IdCardType
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var context = new ContextBase();
            var idCardType = context.IdCards.SingleOrDefault(x => x.Id == id);

            return Ok(idCardType);
        }

        // Post api/users/IdCardType
        [HttpPost]
        [Produces("application/json")]
        public IActionResult Post([FromBody] IdCardType idCardType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _IAppIdCardType.Update(idCardType);

            return Ok(idCardType);
        }

        // Post api/users
        [HttpGet]
        [Produces("application/json")]
        [Route("new")]
        public IActionResult New()
        {
            var final = new
            {
                Years = new List<string> { DateTime.Now.ToString("yyyy"), DateTime.Now.AddYears(1).ToString("yyyy") }
            };

            return Ok(final);
        }
    }
}
