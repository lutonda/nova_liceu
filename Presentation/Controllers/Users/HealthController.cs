﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Interface.Users;
using Domain.Entity.Users;
using Infra.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Presentation.Controllers.Users
{
    [Authorize]
    [Route("Api/Users/[Controller]")]
    public class HealthController : ControllerBase
    {
        private readonly IAppHealth _IAppHealth;
        private readonly IAppBloodGroup _IAppBloodGroup;
        public HealthController(IAppHealth iAppHealth, IAppBloodGroup iAppBloodGroup)
        {
            _IAppHealth = iAppHealth;
            _IAppBloodGroup = iAppBloodGroup;
        }

        // GET api/Users/Health
        [HttpGet]
        [Produces("application/Json")]
        public IActionResult Get()
        {

            var final = new List<Health>();
            var context = new ContextBase();
            var healths = context.Healths.Include(p => p.BloodGroup).ToList();

            foreach (var health in healths)
            {
                final.Add(health);
            }

            return Ok(final);
        }

        // GET api/Users/health
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var context = new ContextBase();
            var health = context.Healths.Include(p => p.BloodGroup).SingleOrDefault(x => x.Id == id);

            return Ok(health);
        }

        // Post api/users/Health
        [HttpPost]
        [Produces("application/json")]
        public IActionResult Post([FromBody] Health health)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _IAppHealth.Update(health);

            return Ok(health);
        }

        // Post api/users
        [HttpGet]
        [Produces("application/json")]
        [Route("new")]
        public IActionResult New()
        {
            var final = new
            {
                BloodGroups = _IAppBloodGroup,
                Years = new List<string> { DateTime.Now.ToString("yyyy"), DateTime.Now.AddYears(1).ToString("yyyy") }
            };

            return Ok(final);
        }
    }
}
