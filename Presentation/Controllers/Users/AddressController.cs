﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Interface.Users;
using Domain.Entity.Users;
using Infra.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Presentation.Controllers.Users
{
    [Authorize]
    [Route("Api/Users/[Controller]")]
    public class AddressController : ControllerBase
    {
        private readonly IAppAddress _IAppAddress;
        private readonly IAppTownship _IAppTownship;

        public AddressController(IAppAddress iAppAddress, IAppTownship iAppTownship)
        {
            _IAppAddress = iAppAddress;
            _IAppTownship = iAppTownship;
        }

        // GET api/Users/Address
        [HttpGet]
        [Produces("application/Json")]
        public IActionResult Get()
        {

            var final = new List<Address>();
            var context = new ContextBase();
            var addresses = context.Adresses.ToList();

            foreach (var address in addresses)
            {
                final.Add(address);
            }

            return Ok(final);
        }

        // GET api/Users/address
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var context = new ContextBase();
            var address = context.Adresses.Include(p => p.Township).SingleOrDefault(x => x.Id == id);

            return Ok(address);
        }

        // Post api/users/address
        [HttpPost]
        [Produces("application/json")]
        public IActionResult Post([FromBody] Address address)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _IAppAddress.Update(address);

            return Ok(address);
        }

        // Post api/users
        [HttpGet]
        [Produces("application/json")]
        [Route("new")]
        public IActionResult New()
        {
            var final = new
            {
                Townships = _IAppTownship,
                Years = new List<string> { DateTime.Now.ToString("yyyy"), DateTime.Now.AddYears(1).ToString("yyyy") }
            };

            return Ok(final);
        }
    }
}
