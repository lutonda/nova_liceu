﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Interface.Users;
using Domain.Entity.Users;
using Infra.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Presentation.Controllers.Users
{
    [Authorize]
    [Route("Api/Users/[Controller]")]
    public class ProvinceController : ControllerBase
    {
        private readonly IAppProvince _IAppProvince;
        private readonly IAppCountry _IAppCountry;
        public ProvinceController(IAppProvince iAppProvince, IAppCountry iAppCountry)
        {
            _IAppProvince = iAppProvince;
            _IAppCountry = iAppCountry;
        }

        // GET api/Users/Province
        [HttpGet]
        [Produces("application/Json")]
        public IActionResult Get()
        {

            var final = new List<Province>();
            var context = new ContextBase();
            var provinces = context.Provinces.Include(p => p.Country).ToList();

            foreach (var province in provinces)
            {
                final.Add(province);
            }

            return Ok(final);
        }

        // GET api/Users/Province
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var context = new ContextBase();
            var province = context.Provinces.Include(p => p.Country).SingleOrDefault(x => x.Id == id);

            return Ok(province);
        }

        // Post api/users/Province
        [HttpPost]
        [Produces("application/json")]
        public IActionResult Post([FromBody] Province province)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _IAppProvince.Update(province);

            return Ok(province);
        }

        // Post api/users
        [HttpGet]
        [Produces("application/json")]
        [Route("new")]
        public IActionResult New()
        {
            var final = new
            {
                Countries = _IAppCountry,
                Years = new List<string> { DateTime.Now.ToString("yyyy"), DateTime.Now.AddYears(1).ToString("yyyy") }
            };

            return Ok(final);
        }
    }
}
