﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Interface.Users;
using Domain.Entity.Users;
using Infra.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Presentation.Controllers.Users
{
    [Authorize]
    [Route("Api/Users/[Controller]")]
    public class BloodGroupController : ControllerBase
    {
        private readonly IAppBloodGroup _IAppBloodGroup;
        public BloodGroupController(IAppBloodGroup iAppBloodGroup)
        {
            _IAppBloodGroup = iAppBloodGroup;
        }

        // GET api/Users/BloodGroup
        [HttpGet]
        [Produces("application/Json")]
        public IActionResult Get()
        {

            var final = new List<BloodGroup>();
            var context = new ContextBase();
            var bloodGroups = context.BloodGroups.ToList();

            foreach (var bloodGroup in bloodGroups)
            {
                final.Add(bloodGroup);
            }

            return Ok(final);
        }

        // GET api/Users/BloodGroup
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var context = new ContextBase();
            var bloodGroup = context.BloodGroups.SingleOrDefault(x => x.Id == id);

            return Ok(bloodGroup);
        }

        // Post api/users/BloodGroup
        [HttpPost]
        [Produces("application/json")]
        public IActionResult Post([FromBody] BloodGroup bloodGroup)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _IAppBloodGroup.Update(bloodGroup);

            return Ok(bloodGroup);
        }

        // Post api/users
        [HttpGet]
        [Produces("application/json")]
        [Route("new")]
        public IActionResult New()
        {
            var final = new
            {
                Years = new List<string> { DateTime.Now.ToString("yyyy"), DateTime.Now.AddYears(1).ToString("yyyy") }
            };

            return Ok(final);
        }
    }
}
