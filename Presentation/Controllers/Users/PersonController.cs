﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Interface.Users;
using Domain.Entity.Users;
using Infra.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Presentation.Controllers.Users
{
    [Authorize]
    [Route("Api/Users/[Controller]")]
    public class PersonController : ControllerBase
    {
        private readonly IAppPerson _IAppPerson;
        private readonly IAppSex _IAppSex;
        private readonly IAppIdCard _IAppIdCard;
        private readonly IAppAddress _IAppAddress;
        private readonly IAppHealth _IAppHealth;
        private readonly IAppTownship _IAppTownship;
        public PersonController(
            IAppPerson iAppPerson,
            IAppSex iAppSex,
            IAppIdCard iAppIdCard, 
            IAppAddress iAppAddress, 
            IAppHealth iAppHealth, 
            IAppTownship iAppTownship
            )
        {
            _IAppPerson = iAppPerson;
            _IAppPerson = iAppPerson;
            _IAppSex = iAppSex;
            _IAppIdCard = iAppIdCard;
            _IAppAddress = iAppAddress;
            _IAppHealth = iAppHealth;
            _IAppTownship = iAppTownship;
        }

        // GET api/Users/Person
        [HttpGet]
        [Produces("application/Json")]
        public IActionResult Get()
        {

            var final = new List<Person>();
            var context = new ContextBase();
            var persons = context.Persons.Include(p => p.Sex).Include(p => p.IdCard)
                .Include(p => p.Address).Include(p => p.Health).Include(p => p.BirthPlace).ToList();

            foreach (var person in persons)
            {
                final.Add(person);
            }

            return Ok(final);
        }

        // GET api/Users/Person
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var context = new ContextBase();
            var person = context.Persons.Include(p => p.Sex).Include(p => p.IdCard)
                .Include(p => p.Address).Include(p => p.Health).Include(p => p.BirthPlace).SingleOrDefault(x => x.Id == id);

            return Ok(person);
        }

        // Post api/users/Person
        [HttpPost]
        [Produces("application/json")]
        public IActionResult Post([FromBody] Person person)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _IAppPerson.Update(person);

            return Ok(person);
        }

        // Post api/users
        [HttpGet]
        [Produces("application/json")]
        [Route("new")]
        public IActionResult New()
        {
            var final = new
            {
                Sexs = _IAppSex,
                IdCards = _IAppIdCard,
                Addresses = _IAppAddress,
                Healths = _IAppHealth,
                Townships = _IAppTownship,
                Years = new List<string> { DateTime.Now.ToString("yyyy"), DateTime.Now.AddYears(1).ToString("yyyy") }
            };

            return Ok(final);
        }
    }
}
