﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Interface.Users;
using Domain.Entity.Users;
using Infra.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Presentation.Controllers.Users
{
    [Authorize]
    [Route("Api/Users/[Controller]")]
    public class ContactController : ControllerBase
    {
        private readonly IAppContact _IAppContact;
        private readonly IAppContactType _IAppContactType;
        public ContactController(IAppContact iAppContact, IAppContactType iAppContactType)
        {
            _IAppContact = iAppContact;
            _IAppContactType = iAppContactType;
            
        }

        // GET api/Users/Contact
        [HttpGet]
        [Produces("application/Json")]
        public IActionResult Get()
        {

            var final = new List<Contact>();
            var context = new ContextBase();
            var contacts = context.Contacts.Include(p => p.ContactType).ToList();

            foreach (var contact in contacts)
            {
                final.Add(contact);
            }

            return Ok(final);
        }

        // GET api/Users/Contact
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var context = new ContextBase();
            var contact = context.Contacts.Include(p => p.ContactType).SingleOrDefault(x => x.Id == id);

            return Ok(contact);
        }

        // Post api/users/Contact
        [HttpPost]
        [Produces("application/json")]
        public IActionResult Post([FromBody] Contact contact)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _IAppContact.Update(contact);

            return Ok(contact);
        }

        // Post api/users
        [HttpGet]
        [Produces("application/json")]
        [Route("new")]
        public IActionResult New()
        {
            var final = new
            {
                ContactTypes = _IAppContactType,
                Years = new List<string> { DateTime.Now.ToString("yyyy"), DateTime.Now.AddYears(1).ToString("yyyy") }
            };

            return Ok(final);
        }
    }
}
