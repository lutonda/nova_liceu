﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Interface.Users;
using Domain.Entity.Users;
using Infra.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Presentation.Controllers.Users
{
    [Authorize]
    [Route("Api/Users/[Controller]")]
    public class ContactTypeController : ControllerBase
    {
        private readonly IAppContactType _IAppContactType;
        public ContactTypeController(IAppContactType iAppContactType)
        {
            _IAppContactType = iAppContactType;
        }

        // GET api/Users/ContactType
        [HttpGet]
        [Produces("application/Json")]
        public IActionResult Get()
        {

            var final = new List<ContactType>();
            var context = new ContextBase();
            var contactTypes = context.ContactTypes.ToList();

            foreach (var contactType in contactTypes)
            {
                final.Add(contactType);
            }

            return Ok(final);
        }

        // GET api/Users/ContactType
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var context = new ContextBase();
            var contactType = context.ContactTypes.SingleOrDefault(x => x.Id == id);

            return Ok(contactType);
        }

        // Post api/users/ContactType
        [HttpPost]
        [Produces("application/json")]
        public IActionResult Post([FromBody] ContactType contactType)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _IAppContactType.Update(contactType);

            return Ok(contactType);
        }

        // Post api/users
        [HttpGet]
        [Produces("application/json")]
        [Route("new")]
        public IActionResult New()
        {
            var final = new
            {
                Years = new List<string> { DateTime.Now.ToString("yyyy"), DateTime.Now.AddYears(1).ToString("yyyy") }
            };

            return Ok(final);
        }
    }
}
