using System;
using System.Collections.Generic;
using Application.Interface;
using Domain.Entity.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using UserModel = Presentation.Models.UserModel;

namespace Presentation.Controllers
{
    [Authorize]
    [Route("Api/[Controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IAppUser _IAppuser;

        public UsersController(IAppUser IAppuser)
        {
            _IAppuser= IAppuser;
        }
        // GET api/users
        [HttpGet]
        [Produces("application/Json")]
        public IActionResult Get()
        {
           // return Ok(new string[] { "value1", "value2" });
            var users = _IAppuser.List();
            var final =new List<UserModel>();
            foreach (var user in users)
            {
                final.Add(new UserModel{Id = user.Id,Name = "",Username = user.Username});
            }
            return Ok(final);
        }

        public User Login(string userName, string passWord)
        {
            return new User();
        }
        // GET api/users/5
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            return Ok(_IAppuser.Show(id));
        }
        
        // Post api/users
        [HttpPost]
        [Produces("application/json")]
        public IActionResult Post([FromBody] User user)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _IAppuser.Add(new User{Username = user.Username});
            
            return Ok(user);
        }
    }
}