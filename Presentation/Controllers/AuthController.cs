using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Application.Interface;
using Infra.Config;
using Microsoft.AspNetCore.Identity.UI.Pages.Account.Internal;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Presentation.Models;

namespace Presentation.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class AuthController : Controller
    {
        private readonly IAppUser iAppUser;

        public AuthController(IAppUser IAppUser)
        {
            iAppUser = IAppUser;
        }
        [HttpPost("token")]
        public IActionResult Token()
        {
            var t= new InitValuesBase();
            //string tokenString = "test";
            var header = Request.Headers["Authorization"];
            if (header.ToString().StartsWith("Basic"))
            {
                var credValue = header.ToString().Substring("Basic ".Length).Trim();
                var usernameAndPassenc = Encoding.UTF8.GetString(Convert.FromBase64String(credValue)); //admin:pass
                var usernameAndPass = usernameAndPassenc.Split(":");
                //check in DB username and pass exist
                
                var currentUser=new UsersController(iAppUser).Login(null, null);
                
                if (usernameAndPass[0] == "Admin" && usernameAndPass[1] == "pass")
                {
                    var user = new UserModel() {Name = "Default User", Username = "Admin"};
                        
                    var claimsdata = new[] { new Claim(ClaimTypes.Name, usernameAndPass[0]) };
                    var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("ahbasshfbsahjfbshajbfhjasbfashjbfsajhfvashjfashfbsahfbsahfksdjf"));
                    var signInCred = new SigningCredentials(key, SecurityAlgorithms.HmacSha256Signature);
                    var token = new JwtSecurityToken(
                        issuer: "mysite.com",
                        audience: "mysite.com",
                        expires: DateTime.Now.AddMinutes(1000),
                        claims: claimsdata,
                        signingCredentials: signInCred
                    );
                    var tokenString = new {access_token= new JwtSecurityTokenHandler().WriteToken(token),userData= user};
                    return Ok(tokenString);
                }
            }
            return BadRequest("wrong request x"+header.ToString());
            
            // return View();
        }
    }
}