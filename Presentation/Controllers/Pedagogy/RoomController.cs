﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Interface.Pedagogy;
using Domain.Entity.Pedagogy;
using Infra.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Presentation.Controllers.Pedagogy
{
    [Authorize]
    [Route("Api/Pedagogy/[Controller]")]
    public class RoomController : ControllerBase
    {
        private readonly IAppRoom _IAppRoom;

        public RoomController(IAppRoom iAppRoom)
        {
            _IAppRoom = iAppRoom;
        }

        // GET api/Pedagogy/Room
        [HttpGet]
        [Produces("application/Json")]
        public IActionResult Get()
        {

            var final = new List<Room>();
            var context = new ContextBase();
            var rooms = context.Rooms.ToList();

            foreach (var room in rooms)
            {
                final.Add(room);
            }

            return Ok(final);
        }

        // GET api/Pedagogy/room
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var context = new ContextBase();
            var room = context.Rooms.SingleOrDefault(x => x.Id == id);

            return Ok(room);
        }

        // Post api/pedagogy/room
        [HttpPost]
        [Produces("application/json")]
        public IActionResult Post([FromBody] Room room)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _IAppRoom.Update(room);

            return Ok(room);
        }

        // Post api/room
        [HttpGet]
        [Produces("application/json")]
        [Route("new")]
        public IActionResult New()
        {
            var final = new
            {
                Years = new List<string> { DateTime.Now.ToString("yyyy"), DateTime.Now.AddYears(1).ToString("yyyy") }
            };

            return Ok(final);
        }
    }
}
