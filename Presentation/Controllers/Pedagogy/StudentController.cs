using System;
using System.Collections.Generic;
using System.Linq;
using Application.Interface.Pedagogy;
using Application.Interface.Users;
using Domain.Entity.Pedagogy;
using Domain.Entity.Users;
using Infra.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Presentation.Controllers.Pedagogy
{
    [Authorize]
    [Route("Api/pedagogy/[Controller]")]
    public class StudentController : ControllerBase
    {
        private readonly IAppStudent _iAppStudent;

        private readonly IAppSex _iAppSex;

        private readonly IAppTownship _iAppTownship;

        private readonly IAppContactType _iAppContactType;
        
        private readonly IAppPerson _iAppPerson;
        
        private readonly IAppIdCardType _iAppIdCardType;

        private readonly IAppProvince _iAppProvince;


        public StudentController(
            IAppStudent iAppStudent,
            IAppContactType iAppContactType,
            IAppProvince iAppProvince,
            IAppTownship iAppTownship,
            IAppSex iAppSex,
            IAppIdCardType iAppIdCardType,
            IAppPerson iAppPerson
        )
        {
            _iAppStudent = iAppStudent;
            _iAppContactType = iAppContactType;
            _iAppSex = iAppSex;
            _iAppTownship = iAppTownship;
            _iAppProvince = iAppProvince;
            _iAppIdCardType = iAppIdCardType;
            _iAppPerson = iAppPerson;
        }

        // GET api/pedagogy/academiclass
        [HttpGet]
        [Produces("application/Json")]
        public IActionResult Get()
        {
            
            var final = new List<Student>();
            var context=new ContextBase();
            var students = context.Students
                    .Include(p => p.Person)
                        .ThenInclude(p=>p.Sex)
                    .Include(p=>p.Classes)
                        .ThenInclude(p=>p.AcademiClass)
                    .ToList();

            foreach (var student in students)
            {
                final.Add(student);
            }

            return Ok(final);
        }

        // GET api/pedagogy/academiclass
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var context = new ContextBase();
            
            var student = context.Students
                    .Include(p=>p.Classes)
                        .ThenInclude(p=>p.AcademiClass)
                            .ThenInclude(p=>p.Grade)
                    .Include(p=>p.Classes)
                    .Include(p => p.Person)
                        .ThenInclude(p => p.Sex)
                    .SingleOrDefault(x => x.Id == id);

            student.Person = context.Persons
                    .Include(p => p.Sex)
                    .Include(p => p.Address)
                        .ThenInclude(p => p.Township)
                        .ThenInclude(p => p.Province)
                    .Include(p => p.Contacts)
                    .Include(p => p.IdCard)
                        .ThenInclude(p => p.IdCardType)
                    .Include(p => p.BirthPlace)
                        .ThenInclude(p => p.Province)
                    .SingleOrDefault(x => x.Id == student.PersonId)
                ;
            

            //professor.Subjects = subjects;
            student.Person.Contacts = context.Contacts
                    .Where(p=> p.PersonId == student.PersonId)
                    .Include(p=>p.ContactType)
                    .ToList();
            return Ok(student);
        }

        // Post api/pedagogy/academiclass
        [HttpPost]
        [Produces("application/json")]
        public IActionResult Post([FromBody] Student student)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            
            _iAppStudent.Update(student);
            
            return Ok(student);
        }

        // Get api/student
        [HttpGet]
        [Produces("application/json")]
        [Route("new")]
        public IActionResult New()
        {


            var context = new ContextBase();

            var final = new
            {
                Sex = _iAppSex.List(),
                Township = context.Townships.Include(p => p.Province).ToList(),
                IdCardType = _iAppIdCardType.List(),
                ContactType = _iAppContactType.List(),
                BloodGroup = context.BloodGroups.ToList(),
            };

            return Ok(final);
        }
    }
}
