﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Interface.Pedagogy;
using Application.Interface.Users;
using Domain.Entity.Pedagogy;
using Domain.Entity.Users;
using Infra.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Presentation.Controllers.Pedagogy
{
    [Authorize]
    [Route("Api/Pedagogy/[Controller]")]
    public class ProfessorController : ControllerBase
    {
        private readonly IAppProfessor _IAppProfessor;
        private readonly IAppPerson _IAppPerson;

        public ProfessorController(IAppProfessor iAppProfessor, IAppPerson iAppPerson)
        {
            _IAppProfessor = iAppProfessor;
            _IAppPerson = iAppPerson;
        }

        // GET api/Pedagogy/Professor
        [HttpGet]
        [Produces("application/Json")]
        public IActionResult Get()
        {
            var final = new List<Professor>();
            var context = new ContextBase();
            var professors = context.Professors.Include(p => p.Person).ToList();

            foreach (var professor in professors)
            {
                try
                {
                    professor.Person.Sex = context.Sexs.SingleOrDefault(x => x.Id == professor.Person.SexId);

                    final.Add(professor);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }

            return Ok(final);
        }

        // GET api/Pedagogy/professor
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var context = new ContextBase();
            
            var professor = context
                .Professors
                .Include(p=>p.Subjects)
                .ThenInclude(p=>p.Class)
                .Include(p=>p.Subjects)
                .ThenInclude(p=>p.Subject)
                .Include(p => p.Person)
                .ThenInclude(p => p.Sex)
                .SingleOrDefault(x => x.Id == id);

            professor.Person = context.Persons
                    .Include(p => p.Sex)
                    .Include(p => p.Address)
                        .ThenInclude(p => p.Township)
                        .ThenInclude(p => p.Province)
                    
                    .Include(p => p.Contacts)
                    .Include(p => p.IdCard)
                        .ThenInclude(p => p.IdCardType)
                    
                    .Include(p => p.BirthPlace)
                        .ThenInclude(p => p.Province)
                    
                    .SingleOrDefault(x => x.Id == professor.PersonId)
                ;
            foreach (var subject in professor.Subjects)
            {
                subject.Class = context.AcademiClasses
                    .Include(p=>p.Grade)
                    .Include(p=>p.Room)
                    .SingleOrDefault(x => x.Id == subject.Class.Id);
            }

            //professor.Subjects = subjects;
            professor.Person.Contacts = context.Contacts.Where(p=> p.PersonId == professor.PersonId).ToList();
            return Ok(professor);
        }

        // Post api/pedagogy/professor
        [HttpPost]
        [Produces("application/json")]
        public IActionResult Post([FromBody] Professor professor)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _IAppProfessor.Update(professor);

            return Ok(professor);
        }

        // Post api/users
        [HttpGet]
        [Produces("application/json")]
        [Route("new")]
        public IActionResult New()
        {
            /*
            this.townships = data.townships;
            this.idCardTypes = data.idCardTypes;
            this.contactTypes = data.contactTypes;
            this.bloodGroups = data.bloodGroups;
            */
            var context = new ContextBase();

            var final = new
            {
                Sex = context.Sexs.ToList(),
                Township = context.Townships.Include(p => p.Province).ToList(),
                IdCardType = context.IdCardTypes.ToList(),
                ContactType = context.ContactTypes.ToList(),
                BloodGroup = context.BloodGroups.ToList(),
            };

            return Ok(final);
        }
    }
}