﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Interface.Pedagogy;
using Domain.Entity.Pedagogy;
using Infra.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Presentation.Controllers.Pedagogy
{
    [Authorize]
    [Route("Api/Pedagogy/[Controller]")]
    public class GradeController : ControllerBase
    {
        private readonly IAppGrade _IAppGrade;

        public GradeController(IAppGrade iAppGrade)
        {
            _IAppGrade = iAppGrade;
        }

        // GET api/Pedagogy/Grade
        [HttpGet]
        [Produces("application/Json")]
        public IActionResult Get()
        {

            var final = new List<Grade>();
            var context = new ContextBase();
            var grades = context.Grades.ToList();

            foreach (var grade in grades)
            {
                final.Add(grade);
            }

            return Ok(final);
        }

        // GET api/Pedagogy/Grade
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var context = new ContextBase();
            var grade = context.Grades.SingleOrDefault(x => x.Id == id);

            return Ok(grade);
        }

        // Post api/pedagogy/grade
        [HttpPost]
        [Produces("application/json")]
        public IActionResult Post([FromBody] Grade grade)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _IAppGrade.Update(grade);

            return Ok(grade);
        }

        // Post api/users
        [HttpGet]
        [Produces("application/json")]
        [Route("new")]
        public IActionResult New()
        {
            var final = new
            {
                Years = new List<string> { DateTime.Now.ToString("yyyy"), DateTime.Now.AddYears(1).ToString("yyyy") }
            };

            return Ok(final);
        }
    }
}
