using System;
using System.Collections.Generic;
using System.Linq;
using Application.Interface.Pedagogy;
using Domain.Entity.Pedagogy;
using Infra.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Presentation.Controllers.Pedagogy
{
    [Authorize]
    [Route("Api/pedagogy/[Controller]")]
    public class AcademiClassController : ControllerBase
    {
        private readonly IAppAcademiClass _IAppAcademiClass;
        private readonly IAppPeriod _IAppPeriod;
        private readonly IAppGrade _IAppGrade;
        private readonly IAppProfessor _IAppProfessor;
        private readonly IAppRoom _IAppRoom;
        private readonly IAppSubject _iAppSubject;

        public AcademiClassController(
            IAppPeriod IAppPeriod,
            IAppGrade IAppGrade,
            IAppAcademiClass IAppAcademiClass,
            IAppRoom IAppRoom,
            IAppSubject iAppSubject,
            IAppProfessor IAppProfessor
        )
        {
            _IAppPeriod = IAppPeriod;
            _IAppGrade = IAppGrade;
            _IAppAcademiClass = IAppAcademiClass;
            _IAppRoom = IAppRoom;
            _iAppSubject = iAppSubject;
            _IAppProfessor = IAppProfessor;
        }

        // GET api/pedagogy/academiclass
        [HttpGet]
        [Produces("application/Json")]
        public IActionResult Get()
        {
            var final = new List<AcademiClass>();
            var context = new ContextBase();
            var academicClasses = context.AcademiClasses
                .Include(p => p.Period)
                .Include(p => p.Grade)
                .Include(p=>p.students)
                .Include(p=>p.Room)
                .ToList();

            foreach (var academicClass in academicClasses)
            {
                final.Add(academicClass);
            }

            return Ok(final);
        }

        // GET api/pedagogy/academiclass
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var context = new ContextBase();
            var academicClasse = context.AcademiClasses
                .Include(p => p.Period)
                .Include(p => p.Grade)
                .Include(p => p.Room)
                .Include(p => p.Subjects)
                .ThenInclude(p=>p.Subject)
                .Include(p=>p.Subjects)
                .ThenInclude(p=>p.Professor)
                .ThenInclude(p=>p.Person)
                .Include(p=>p.students)
                .ThenInclude(p=>p.Student.Person)
                .ThenInclude(p=>p.Sex)
                .SingleOrDefault(x => x.Id == id);

           /* var subjects = context.AlocatedSubjects
                .Where(p => p.Class.Id == academicClasse.Id)
                .Include(p => p.Subject)
                .Include(p => p.Professor)
                .ToList();

           /* foreach (var subject in academicClasse.Subjects)
            {
                subject.Professor.Person = context.Persons.SingleOrDefault(x => x.Id == subject.Professor.PersonId);
            }
            /*foreach (var registration in academicClasse.students)
            {
                registration.Student.Person = context.Persons.Include(p => p.Sex)
                    .SingleOrDefault(x => x.Id == registration.Student.PersonId);
            }*/
/*
            academicClasse.Subjects = subjects;
            academicClasse.students = registrations;
*/
            return Ok(academicClasse);
        }

        // Post api/pedagogy/academiclass
        [HttpPost]
        [Produces("application/json")]
        public IActionResult Post([FromBody] AcademiClass academiClass)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            /*academiClass.LiderId = null;
            academiClass.Grade = null;
            academiClass.Room = null;
            academiClass.Period = null;*/
            _IAppAcademiClass.Update(academiClass);

            return Ok(academiClass);
        }

        // Post api/users
        [HttpGet]
        [Produces("application/json")]
        [Route("new")]
        public IActionResult New()
        {
            var context = new ContextBase();
            List<Professor> professors = _IAppProfessor.List();

            foreach (var professor in professors)
            {
                professor.Person = context.Persons.SingleOrDefault(x => x.Id == professor.PersonId);
            }

            var final = new
            {
                Grades = _IAppGrade.List(),
                Periods = _IAppPeriod.List(),
                Room = _IAppRoom.List(),
                Subjects = _iAppSubject.List(),
                Years = new List<string> {DateTime.Now.ToString("yyyy"), DateTime.Now.AddYears(1).ToString("yyyy")},
                Professors = professors
            };

            return Ok(final);
        }
    }
}