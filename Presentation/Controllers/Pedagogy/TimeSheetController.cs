﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Interface.Pedagogy;
using Domain.Entity.Pedagogy;
using Infra.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Presentation.Controllers.Pedagogy
{
    [Authorize]
    [Route("Api/Pedagogy/[Controller]")]
    public class TimeSheetController : ControllerBase
    {
        private readonly IAppTimeSheet _IAppTimeSheet;
        private readonly IAppTimeTable _IAppTimeTable;
        private readonly IAppRegistrationClass _IAppRegistrationClass;

        public TimeSheetController(IAppTimeSheet iAppTimeSheet, IAppTimeTable iAppTimeTable, IAppRegistrationClass iAppRegistrationClass)
        {
            _IAppTimeSheet = iAppTimeSheet;
            _IAppTimeTable = iAppTimeTable;
            _IAppRegistrationClass = iAppRegistrationClass;
        }

        // GET api/Pedagogy/TimeSheet
        [HttpGet]
        [Produces("application/Json")]
        public IActionResult Get()
        {

            var final = new List<TimeSheet>();
            var context = new ContextBase();
            var timeSheets = context.TimeSheets.Include(p => p.TimeTable).Include(p => p.RegistrationClass).ToList();

            foreach (var timeSheet in timeSheets)
            {
                final.Add(timeSheet);
            }

            return Ok(final);
        }

        // GET api/Pedagogy/TimeSheet
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var context = new ContextBase();
            var timeSheet = context.TimeSheets.Include(p => p.TimeTable).Include(p => p.RegistrationClass).SingleOrDefault(x => x.Id == id);

            return Ok(timeSheet);
        }

        // Post api/pedagogy/TimeSheet
        [HttpPost]
        [Produces("application/json")]
        public IActionResult Post([FromBody] TimeSheet timeSheet)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _IAppTimeSheet.Update(timeSheet);

            return Ok(timeSheet);
        }

        // Post api/users
        [HttpGet]
        [Produces("application/json")]
        [Route("new")]
        public IActionResult New()
        {
            var final = new
            {
                TimeTables = _IAppTimeTable,
                RegistrationClasss = _IAppRegistrationClass,
                Years = new List<string> { DateTime.Now.ToString("yyyy"), DateTime.Now.AddYears(1).ToString("yyyy") }
            };

            return Ok(final);
        }
    }
}
