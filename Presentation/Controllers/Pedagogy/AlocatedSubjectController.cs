﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Interface.Pedagogy;
using Domain.Entity.Pedagogy;
using Infra.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Presentation.Controllers.Pedagogy
{
    [Authorize]
    [Route("Api/Pedagogy/[Controller]")]
    public class AlocatedSubjectController : ControllerBase
    {
        private readonly IAppAlocatedSubject _IAppAlocatedSubject;
        private readonly IAppAcademiClass _IAppAcademiClass;
        private readonly IAppSubject _IAppSubject;
        private readonly IAppProfessor _IAppProfessor;

        public AlocatedSubjectController(
            IAppAlocatedSubject AlocatedSubject,
            IAppAcademiClass IAppAcademiClass,
            IAppSubject iAppSubject,
            IAppProfessor IAppProfessor
        )
        {
            _IAppAlocatedSubject = AlocatedSubject;
            _IAppAcademiClass = IAppAcademiClass;
            _IAppSubject = iAppSubject;
            _IAppProfessor = IAppProfessor;
        }

        // GET api/pedagogy/alocatedsubject
        [HttpGet]
        [Produces("application/Json")]
        public IActionResult Get()
        {

            var final = new List<AlocatedSubject>();
            var context = new ContextBase();
            var alocatedSubjects = context.AlocatedSubjects.Include(p => p.Class).Include(p => p.Subject).Include(p => p.Professor).ToList();

            foreach (var alocatedSubject in alocatedSubjects)
            {
                final.Add(alocatedSubject);
            }

            return Ok(final);
        }

        // GET api/pedagogy/alocatedsubject
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var context = new ContextBase();
            var alocatedSubjects = context.AlocatedSubjects.Include(p => p.Class).Include(p => p.Subject).Include(p => p.Professor)
                .SingleOrDefault(x => x.Id == id);

            return Ok(alocatedSubjects);
        }

        // Post api/pedagogy/alocatedsubject
        [HttpPost]
        [Produces("application/json")]
        public IActionResult Post([FromBody] AlocatedSubject alocatedSubject)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            alocatedSubject.Class = null;
            alocatedSubject.Subject = null;
            alocatedSubject.Professor = null;
            _IAppAlocatedSubject.Update(alocatedSubject);

            return Ok(alocatedSubject);
        }

        // Post api/users
        [HttpGet]
        [Produces("application/json")]
        [Route("new")]
        public IActionResult New()
        {
            var final = new
            {
                Classs = _IAppAcademiClass.List(),
                Subjects = _IAppSubject.List(),
                Professors = _IAppProfessor.List(),
                Years = new List<string> { DateTime.Now.ToString("yyyy"), DateTime.Now.AddYears(1).ToString("yyyy") }
            };

            return Ok(final);
        }
    }
}
