﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Interface.Pedagogy;
using Domain.Entity.Pedagogy;
using Infra.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Presentation.Controllers.Pedagogy
{
    [Authorize]
    [Route("Api/Pedagogy/[Controller]")]
    public class TimeTableController : ControllerBase
    {
        private readonly IAppTimeTable _IAppTimeTable;
        private readonly IAppAlocatedSubject _IAppAlocatedSubject;

        public TimeTableController(IAppTimeTable iAppTimeTable, IAppAlocatedSubject iAppAlocatedSubject)
        {
            _IAppTimeTable = iAppTimeTable;
            _IAppAlocatedSubject = iAppAlocatedSubject;
        }

        // GET api/Pedagogy/TimeTable
        [HttpGet]
        [Produces("application/Json")]
        public IActionResult Get()
        {

            var final = new List<TimeTable>();
            var context = new ContextBase();
            var timeTables = context.TimeTables.Include(p => p.Subject).ToList();

            foreach (var timeTable in timeTables)
            {
                final.Add(timeTable);
            }

            return Ok(final);
        }

        // GET api/Pedagogy/TimeTable
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var context = new ContextBase();
            var timeTable = context.TimeTables.Include(p => p.Subject).SingleOrDefault(x => x.Id == id);

            return Ok(timeTable);
        }

        // Post api/pedagogy/TimeTable
        [HttpPost]
        [Produces("application/json")]
        public IActionResult Post([FromBody] TimeTable timeTable)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _IAppTimeTable.Update(timeTable);

            return Ok(timeTable);
        }

        // Post api/users
        [HttpGet]
        [Produces("application/json")]
        [Route("new")]
        public IActionResult New()
        {
            var final = new
            {
                Subjects = _IAppAlocatedSubject,
                Years = new List<string> { DateTime.Now.ToString("yyyy"), DateTime.Now.AddYears(1).ToString("yyyy") }
            };

            return Ok(final);
        }
    }
}
