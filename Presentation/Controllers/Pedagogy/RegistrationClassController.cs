using System;
using System.Collections.Generic;
using System.Linq;
using Application.Interface.Pedagogy;
using Domain.Entity.Pedagogy;
using Infra.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Presentation.Controllers.Pedagogy
{
    [Authorize]
    [Route("Api/pedagogy/[Controller]")]
    public class RegistrationClassController : ControllerBase
    {
        private readonly IAppAcademiClass _iAppAcademiClass;

        private readonly IAppStudent _iAppStudent;

        private readonly IAppRegistrationClass _iRegistrationClass;

        public RegistrationClassController(IAppAcademiClass iAppAcademiClass, IAppStudent iAppStudent,
            IAppRegistrationClass iRegistrationClass
        )
        {
            _iAppAcademiClass = iAppAcademiClass;
            _iAppStudent = iAppStudent;
            _iRegistrationClass = iRegistrationClass;
        }

        // GET api/pedagogy/academiclass
        [HttpGet]
        [Produces("application/Json")]
        public IActionResult Get()
        {
            var final = new List<AcademiClass>();
            var context = new ContextBase();
            var academicClasses = context.AcademiClasses.Include(p => p.Period).Include(p => p.Grade).ToList();

            foreach (var academicClass in academicClasses)
            {
                final.Add(academicClass);
            }

            return Ok(final);
        }

        // GET api/pedagogy/academiclass
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var context = new ContextBase();
            var academicClasse = context.AcademiClasses.Include(p => p.Period).Include(p => p.Grade)
                .Include(p => p.Room)
                .SingleOrDefault(x => x.Id == id);

            return Ok(academicClasse);
        }

        // Post api/pedagogy/academiclass
        [HttpPost]
        [Produces("application/json")]
        [HttpGet("{id}")]
        [HttpGet("{data}")]
        public IActionResult Post([FromBody] RegistrationClass registrationClass)
        {
            /*registrationClass.AcademiClass = null;
            registrationClass.Student = null;
            */
            var context = new ContextBase();


            var registration = new RegistrationClass();
            registration.StudentId = registrationClass.Student.Id;
            registration.AcademiClassId = registrationClass.AcademiClass.Id;
            
            _iRegistrationClass.Add(registration);
            
            registrationClass.Id = registration.Id;

            //_iRegistrationClass.Add(registrationClass);

            return Ok(registrationClass);
        }

        // Post api/users
        [HttpGet]
        [Produces("application/json")]
        [Route("new")]
        public IActionResult New()
        {
            return Ok(null);
        }
    }
}