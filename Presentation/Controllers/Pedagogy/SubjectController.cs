using System;
using System.Collections.Generic;
using System.Linq;
using Application.Interface.Pedagogy;
using Domain.Entity.Pedagogy;
using Infra.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Presentation.Controllers.Pedagogy
{
    [Authorize]
    [Route("Api/pedagogy/[Controller]")]
    public class PaymentSlipController : ControllerBase
    {
        private readonly IAppSubject _IAppSubject;
        private readonly IAppPeriod _IAppPeriod;
        private readonly IAppGrade _IAppGrade;
        private readonly IAppProfessor _IAppProfessor;
        private readonly IAppRoom _IAppRoom;

        public PaymentSlipController(
            IAppPeriod IAppPeriod,
            IAppGrade IAppGrade,
            IAppSubject IAppSubject,
            IAppRoom IAppRoom,
            IAppProfessor IAppProfessor
        )
        {
            _IAppPeriod = IAppPeriod;
            _IAppGrade = IAppGrade;
            _IAppSubject = IAppSubject;
            _IAppRoom = IAppRoom;
            _IAppProfessor = IAppProfessor;
        }

        // GET api/pedagogy/academiclass
        [HttpGet]
        [Produces("application/Json")]
        public IActionResult Get()
        {
            
            var final = new List<Subject>();
            var context = new ContextBase();
            var academicClasses = context.Subjects.ToList();

            foreach (var academicClass in academicClasses)
            {
                final.Add(academicClass);
            }

            return Ok(final);
        }

        // GET api/pedagogy/academiclass
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var context = new ContextBase();
            var academicClasse = "";
            return Ok(academicClasse);
        }

        // Post api/pedagogy/academiclass
        [HttpPost]
        [Produces("application/json")]
        public IActionResult Post([FromBody] Subject academiClass)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _IAppSubject.Add(academiClass);
            
            return Ok(academiClass);
        }

        // Post api/users
        [HttpGet]
        [Produces("application/json")]
        [Route("new")]
        public IActionResult New()
        {
            var final = new
            {
                Grades = _IAppGrade.List(),
                Periods = _IAppPeriod.List(),
                Room = _IAppRoom.List(),
                Years = new List<string> {DateTime.Now.ToString("yyyy"), DateTime.Now.AddYears(1).ToString("yyyy")}
            };

            return Ok(final);
        }
    }
}
