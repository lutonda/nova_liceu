﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Interface.Pedagogy;
using Domain.Entity.Pedagogy;
using Infra.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Presentation.Controllers.Pedagogy
{
    [Authorize]
    [Route("Api/Pedagogy/[Controller]")]
    public class PeriodController : ControllerBase
    {
        private readonly IAppPeriod _IAppPeriod;

        public PeriodController(IAppPeriod iAppPeriod)
        {
            _IAppPeriod = iAppPeriod;
        }

        // GET api/Pedagogy/Period
        [HttpGet]
        [Produces("application/Json")]
        public IActionResult Get()
        {

            var final = new List<Period>();
            var context = new ContextBase();
            var periods = context.Periods.ToList();

            foreach (var period in periods)
            {
                final.Add(period);
            }

            return Ok(final);
        }

        // GET api/Pedagogy/period
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var context = new ContextBase();
            var period = context.Periods.SingleOrDefault(x => x.Id == id);

            return Ok(period);
        }

        // Post api/pedagogy/period
        [HttpPost]
        [Produces("application/json")]
        public IActionResult Post([FromBody] Period period)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _IAppPeriod.Update(period);

            return Ok(period);
        }

        // Post api/users
        [HttpGet]
        [Produces("application/json")]
        [Route("new")]
        public IActionResult New()
        {
            var final = new
            {
                Years = new List<string> { DateTime.Now.ToString("yyyy"), DateTime.Now.AddYears(1).ToString("yyyy") }
            };

            return Ok(final);
        }
    }
}
