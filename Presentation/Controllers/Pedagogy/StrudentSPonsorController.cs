﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Interface.Pedagogy;
using Application.Interface.Users;
using Domain.Entity.Pedagogy;
using Infra.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Presentation.Controllers.Pedagogy
{
    [Authorize]
    [Route("Api/Pedagogy/[Controller]")]
    public class StrudentSponsorController : ControllerBase
    {
        private readonly IAppStrudentSponsor _IAppStrudentSponsor;
        private readonly IAppPerson _IAppPerson;

        public StrudentSponsorController(IAppStrudentSponsor iAppStrudentSponsor, IAppPerson iAppPerson)
        {
            _IAppStrudentSponsor = iAppStrudentSponsor;
            _IAppPerson = iAppPerson;
        }

        // GET api/Pedagogy/StrudentSponsor
        [HttpGet]
        [Produces("application/Json")]
        public IActionResult Get()
        {

            var final = new List<StrudentSponsor>();
            var context = new ContextBase();
            var strudentSponsors = context.StrudentSponsors.Include(p => p.person).ToList();

            foreach (var strudentSponsor in strudentSponsors)
            {
                final.Add(strudentSponsor);
            }

            return Ok(final);
        }

        // GET api/Pedagogy/StrudentSponsor
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var context = new ContextBase();
            var strudentSponsor = context.StrudentSponsors.SingleOrDefault(x => x.Id == id);

            return Ok(strudentSponsor);
        }

        // Post api/pedagogy/StrudentSponsor
        [HttpPost]
        [Produces("application/json")]
        public IActionResult Post([FromBody] StrudentSponsor strudentSponsor)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _IAppStrudentSponsor.Update(strudentSponsor);

            return Ok(strudentSponsor);
        }

        // Post api/users
        [HttpGet]
        [Produces("application/json")]
        [Route("new")]
        public IActionResult New()
        {
            var final = new
            {
                Persons = _IAppPerson,
                Years = new List<string> { DateTime.Now.ToString("yyyy"), DateTime.Now.AddYears(1).ToString("yyyy") }
            };

            return Ok(final);
        }
    }
}
