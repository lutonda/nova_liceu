﻿using System;
using System.Collections.Generic;
using System.Linq;
using Application.Interface.Pedagogy;
using Domain.Entity.Pedagogy;
using Infra.Config;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Presentation.Controllers.Pedagogy
{
    [Authorize]
    [Route("Api/Pedagogy/[Controller]")]
    public class ScoreController : ControllerBase
    {
        private readonly IAppScore _IAppScore;
        private readonly IAppRegistrationClass _IAppRegistrationClass;
        private readonly IAppAlocatedSubject _IAppAlocatedSubject;

        public ScoreController(IAppScore iAppScore, IAppRegistrationClass iAppRegistrationClass, IAppAlocatedSubject iAppAlocatedSubject)
        {
            _IAppScore = iAppScore;
            _IAppRegistrationClass = iAppRegistrationClass;
            _IAppAlocatedSubject = iAppAlocatedSubject;
        }

        // GET api/Pedagogy/Score
        [HttpGet]
        [Produces("application/Json")]
        public IActionResult Get()
        {

            var final = new List<Score>();
            var context = new ContextBase();
            var scores = context.Scores.Include(p => p.Registration).Include(p => p.Subject).ToList();

            foreach (var score in scores)
            {
                final.Add(score);
            }

            return Ok(final);
        }

        // GET api/Pedagogy/score
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
            var context = new ContextBase();
            var score = context.Scores.SingleOrDefault(x => x.Id == id);

            return Ok(score);
        }

        // Post api/pedagogy/score
        [HttpPost]
        [Produces("application/json")]
        public IActionResult Post([FromBody] Score score)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _IAppScore.Update(score);

            return Ok(score);
        }

        // Post api/users
        [HttpGet]
        [Produces("application/json")]
        [Route("new")]
        public IActionResult New()
        {
            var final = new
            {
                Registrations = _IAppRegistrationClass,
                Subjects = _IAppAlocatedSubject,
                Years = new List<string> { DateTime.Now.ToString("yyyy"), DateTime.Now.AddYears(1).ToString("yyyy") }
            };

            return Ok(final);
        }
    }
}
