using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Entity.Pedagogy;

namespace Domain.Entity.Finance
{
    public class PaymentModel : DomEntity
    {
    
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        
        [DataType(DataType.Currency)]
        [Range(typeof(float), "0", "999999999999")]
        [Required(ErrorMessage = "Enter the amount")]
        public float Amount { get; set; }

        public RegistrationClassModel Registration { get; set; }
        
        public string Descriptions { get; set; }

        public PaymentRulesModel Rule { get; set; }


    }
}