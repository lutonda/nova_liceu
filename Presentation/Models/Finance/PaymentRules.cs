using Domain.Entity.Pedagogy;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entity.Finance
{
    public class PaymentRulesModel : DomEntity
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        
        public AcademicClassModel Class { get; set; }

        [Required(ErrorMessage = "Enter the Name of the Payment Rules")]
        public string Name { get; set; }

        public string Descriptions { get; set; }

        [DataType(DataType.Currency)]
        [Range(typeof(float), "0", "999999999999")]
        [Required(ErrorMessage = "Enter the amount")]
        public float Amount { get; set; }
    }
}