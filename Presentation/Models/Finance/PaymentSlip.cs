using Domain.Entity.Pedagogy;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entity.Finance
{
    public class PaymentSlipModel : DomEntity
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        
        public RegistrationClassModel registration { get; set; }

        [DataType(DataType.Date)]
        public DateTime dueDate { get; set; }

        [DataType(DataType.Date)]
        public float addonValue { get; set; }

        public PaymentModel payment { get; set; }

        public string descriptions { get; set; }
        
        
        
    }
}