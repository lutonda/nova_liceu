using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entity.Pedagogy
{
    public class TimeSheetModel : DomEntity
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        
        public TimeTableModel TimeTable { get; set; }
        
        public RegistrationClassModel RegistrationClass { get; set; }

        public int Start { get; set; }

        public int End { get; set; }
    }
}