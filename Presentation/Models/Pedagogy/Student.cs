using Domain.Entity.Users;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entity.Pedagogy
{
    public class StudentModel : DomEntity
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        
        public virtual PersonModel Person { get; set; }
        public Guid? PersonId { get; set; }
        
        public List<RegistrationClassModel> RegistrationClasses { get; set; }
        
    }
}