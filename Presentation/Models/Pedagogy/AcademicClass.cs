using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entity.Pedagogy
{
    public class AcademicClassModel : DomEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        
        [Required(ErrorMessage = "Enter Academic Class name")]
        public string Name { get; set; }

        public string Number { get; set; }

        public GradeModel Grade { get; set; }
        
        public PeriodModel Period { get; set; }
        
        public int Year { get; set; }
        
        public DateTime CreatedDate { get; set; }

        public DateTime ModifiedDate { get; set; }
    }
}