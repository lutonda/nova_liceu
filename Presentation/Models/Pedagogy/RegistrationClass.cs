using Domain.Entity.Finance;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entity.Pedagogy
{
    public class RegistrationClassModel : DomEntity
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        
        public virtual AcademicClassModel AcademiClass { get; set; }
        public Guid? AcademiClassIdModel { get; set; }

        public virtual StudentModel Student { get; set; }
        public Guid? StudentId { get; set; }

        public List<PaymentModel> Payments { get; set; }

        public List<ScoreModel> Scores { get; set; }

        public List<TimeSheetModel> TimeSheets { get; set; }

    }
}