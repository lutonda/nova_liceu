using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entity.Pedagogy
{
    public class RoomModel : DomEntity
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        
        [Required(ErrorMessage ="Enter the name of Room")]
        public string Name { get; set; }

        public string Number { get; set; }
        
        public int Size { get; set; }
    }
}