using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entity.Pedagogy
{
    public class ScoreModel : DomEntity
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        
        public RegistrationClassModel Registration { get; set; }

        public float Value { get; set; }

        public AlocatedSubjectModel Subject { get; set; }
    }
}