using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entity.Pedagogy
{
    public class SubjectModel : DomEntity
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }

        [Required(ErrorMessage ="Enter the Name")]
        public string Name { get; set; }

        public ProfessorModel Leder { get; set; }
        public Guid? LederId { get; set; }
    }
}