using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entity.Pedagogy
{
    public class AlocatedSubjectModel : DomEntity
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        
        public AcademicClassModel Class { get; set; }

        public SubjectModel Subject { get; set; }

        public ProfessorModel Professor { get; set; }
    }
}