using System;

namespace Presentation.Models
{
    public class DomEntity
    {
        public DomEntity()
        {
            this.CreatedDate  = DateTime.UtcNow;
            
            this.ModifiedDate = DateTime.UtcNow;
        }

        public DateTime ModifiedDate { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}