using Domain.Entity.Pedagogy;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entity.Library
{
    public class ObjectAccessibleRulesModel : DomEntity
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        
        [Required(ErrorMessage = "Enter the Name of the Object Accessible Rules")]
        public string name { get; set; }

        public List<RegistrationClassModel> registrations { get; set; }

        public List<PeriodModel> periods { get; set; }

        public List<GradeModel> grades { get; set; }
    }
}