using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Entity.Pedagogy;

namespace Domain.Entity.Library
{
    public class ObjectModel : DomEntity
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        
        [Required(ErrorMessage ="Enter the Name of the Object")]
        public string name { get; set; }

        public ObjectTypeModel type { get; set; }
        
        public string path { get; set; }
        
        public ObjectAccessibleRulesModel rules { get; set; }
        
        public List<string> tags { get; set; }
        [DataType(DataType.Date)]
        public DateTime dueDate { get; set; }

        public List<SubjectModel> subjects { get; set; }
    }
}