using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entity.Library
{
    public class ObjectTypeModel : DomEntity
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        
        [Required(ErrorMessage ="Enter the Name of the Object Type")]
        public string name { get; set; }
        
        public string descriptions { get; set; }
    }
}