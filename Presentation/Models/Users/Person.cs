using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entity.Users
{
    public class PersonModel : DomEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string MidlleName { get; set; }

        public string LastName { get; set; }

        public Sex Sex { get; set; }

        public IdCard IdCard { get; set; }

        public List<Contact> Contacts { get; set; }

        public Adress Adress { get; set; }
        
        public Health Health { get; set; }

        public DateTime BirthDate { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime ModifiedDate { get; set; }
    }
}