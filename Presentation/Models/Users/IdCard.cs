using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entity.Users
{
    public class IdCardModel : DomEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        
        public IdCardType IdCardType { get; set; }
        
        public string IdCardNumber { get; set; }

        public DateTime IdCarDateCreation { get; set; }
        
        public string IdCardLocalCreation { get; set; }
    }
}