using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entity.Users
{
    public class Health : DomEntity
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        
        public virtual BloodGroup BloodGroup { get; set; }
        public Guid? BloodGroupId { get; set; }

        public string SpecialCare{ get; set; }
        
        public string Allergies { get; set; }
    }
}