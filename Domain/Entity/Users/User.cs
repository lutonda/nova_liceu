using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entity.Users
{
    public class User : DomEntity
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
       
        [MinLength(6)]
        public string Username { get; set; }
       
        [MaxLength] 
        public string Passord { get; set; }

    }
}