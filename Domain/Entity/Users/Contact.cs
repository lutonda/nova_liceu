using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entity.Users
{
    public class Contact : DomEntity
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }

        public string Description { get; set; }

        public virtual Guid PersonId { get; set; }
        public virtual ContactType ContactType { get; set; }
        public Guid? ContactTypeId { get; set; }
    }
}