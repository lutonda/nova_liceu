using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entity.Users
{
    public class Person : DomEntity
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }

        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public virtual Sex Sex { get; set; }
        public Guid? SexId { get; set; }

        public virtual IdCard IdCard { get; set; }
        public Guid? IdCardId { get; set; } 
        
        public List<Contact> Contacts { get; set; }

        public virtual Address Address { get; set; }
        public Guid? AddressId { get; set; }
        
        public virtual Health Health { get; set; }
        public Guid? HealthId { get; set; }

        public DateTime BirthDate { get; set; }

        public virtual Township BirthPlace { get; set; }
        public Guid? TownshipId { get; set; }
        
        [Column(TypeName = "ntext")]
        [MaxLength] 
        public string Avatar { get; set; }
    }
}