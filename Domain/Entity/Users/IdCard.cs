using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entity.Users
{
    public class IdCard : DomEntity
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        
        public string IdCardNumber { get; set; }
        
        public virtual  IdCardType IdCardType { get; set; }
        public Guid? IdCardTypeId { get; set; }
        
        public virtual Province IdCardLocalCreation { get; set; }
        public Guid? IdCardLocalCreationId { get; set; }
    }
}