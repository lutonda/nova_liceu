using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entity.Users
{
    public class Province: DomEntity
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        
        public string Name { get; set; }

        public string Code {get; set; }
        
        public virtual Country Country { get; set; }
        public Guid? CountryId { get; set; }
    }
}