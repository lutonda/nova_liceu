using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Entity.Users;

namespace Domain.Entity
{
    public class DomEntity
    {
        public DomEntity()
        {
            this.CreatedDate  = DateTime.UtcNow;
            
            this.ModifiedDate = DateTime.UtcNow;

            this.IsActive = true;

            this.LogicalExclusion = false;
            
        }

        
        public int Code { get; set; }
        
        public bool IsActive  { get; set; }

        public bool LogicalExclusion { get; set; }
        
        public DateTime ModifiedDate { get; set; }

        public DateTime CreatedDate { get; set; }

        public User CreatedUser { get; set; }
    }
}