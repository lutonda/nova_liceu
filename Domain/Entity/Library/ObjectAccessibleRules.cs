using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Entity.Pedagogy;

namespace Domain.Entity.Library
{
    public class ObjectAccessibleRules : DomEntity
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        
        public string name { get; set; }

        public List<RegistrationClass> registrations { get; set; }

        public List<Period> periods { get; set; }

        public List<Grade> grades { get; set; }
    }
}