using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Entity.Pedagogy;

namespace Domain.Entity.Library
{
    public class Object : DomEntity
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        
        public string name { get; set; }

        public ObjectType type { get; set; }
        
        public string path { get; set; }
        
        public ObjectAccessibleRules rules { get; set; }
        
        public string tags { get; set; }
        
        public DateTime dueDate { get; set; }

        public List<Subject> subjects { get; set; }
    }
}