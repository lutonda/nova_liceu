using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Entity.Users;

namespace Domain.Entity.Pedagogy
{
    public class Student : DomEntity
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        
        public virtual Person Person { get; set; }
        public Guid? PersonId { get; set; }
        
        public List<RegistrationClass> Classes { get; set; }
        
    }
}