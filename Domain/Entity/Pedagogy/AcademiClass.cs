using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entity.Pedagogy
{
    public class AcademiClass : DomEntity
    {

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        
        public string Name { get; set; }

        public string Number { get; set; }

        public virtual Grade Grade { get; set; }
        public Guid? GradeId {get;set;}
        
        public virtual Period Period { get; set; }
        public Guid? PeriodId { get; set; }
        
        public virtual Room Room { get; set; }
        public Guid? RoomId { get; set; }
        
        public virtual List<RegistrationClass> students { get; set; }
        
        public virtual List<AlocatedSubject> Subjects { get; set; }
        
        public virtual Professor Lider { get; set; }
        public Guid? LiderId { get; set; }
        
        public int Year { get; set; }
    }
}