using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Entity.Finance;

namespace Domain.Entity.Pedagogy
{
    public class RegistrationClass : DomEntity
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        
        public virtual AcademiClass AcademiClass { get; set; }
        public Guid AcademiClassId { get; set; }
        public virtual Student Student { get; set; }
        public Guid StudentId { get; set; }
       /* public List<Payment> Payments { get; set; }

        public List<Score> Scores { get; set; }

        public List<TimeSheet> TimeSheets { get; set; }*/
        
    }
}