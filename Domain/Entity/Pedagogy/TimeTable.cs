using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entity.Pedagogy
{
    public class TimeTable : DomEntity
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        
        public int Day { get; set; }

        public DateTime Time { get; set; }

        public AlocatedSubject Subject { get; set; }
        
    }
}