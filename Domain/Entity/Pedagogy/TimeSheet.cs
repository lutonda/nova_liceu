using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Domain.Entity.Pedagogy
{
    public class TimeSheet
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        
        public TimeTable TimeTable { get; set; }
        
        public RegistrationClass RegistrationClass { get; set; }

        public int Start { get; set; }

        public int End { get; set; }
    }
}