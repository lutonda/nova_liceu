using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Entity.Users;

namespace Domain.Entity.Pedagogy
{
    public class StrudentSponsor : DomEntity
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        
        public Person person { get; set; }

        public List<Student> Type { get; set; }
    }
}