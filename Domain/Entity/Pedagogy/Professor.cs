using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Entity.Users;

namespace Domain.Entity.Pedagogy
{
    public class Professor: DomEntity
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        
        public virtual Person Person { get; set; }
        
        public Guid? PersonId { get; set; }
        
        public virtual List<AlocatedSubject> Subjects { get; set; }
        
    }
}