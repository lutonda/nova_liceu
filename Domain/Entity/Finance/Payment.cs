using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Entity.Pedagogy;

namespace Domain.Entity.Finance
{
    public class Payment : DomEntity
    {
    
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        
        public float Amount { get; set; }

        public RegistrationClass Registration { get; set; }
        
        public string Descriptions { get; set; }

        public PaymentRules Rule { get; set; }
    }
}