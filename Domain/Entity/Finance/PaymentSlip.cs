using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Entity.Pedagogy;

namespace Domain.Entity.Finance
{
    public class PaymentSlip : DomEntity
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        

        public RegistrationClass registration { get; set; }

        public DateTime dueDate { get; set; }

        public float addonValue { get; set; }

        public Payment payment { get; set; }

        public string descriptions { get; set; }
        
        
        
    }
}