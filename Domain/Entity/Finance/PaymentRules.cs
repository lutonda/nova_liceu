using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Domain.Entity.Pedagogy;

namespace Domain.Entity.Finance
{
    public class PaymentRules : DomEntity
    {
        
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public Guid Id { get; set; }
        
        public AcademiClass Class { get; set; }

        public string Name { get; set; }

        public string Descriptions { get; set; }

        public float Amount { get; set; }
    }
}