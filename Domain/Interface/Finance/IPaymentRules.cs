﻿using Domain.Entity.Finance;
using Domain.Interface.Generic;


namespace Domain.Interface.Finance
{
    public interface IPaymentRules : InterfaceGeneric<PaymentRules>
    {
    }
}
