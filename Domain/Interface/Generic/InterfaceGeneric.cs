using System;
using System.Collections.Generic;

namespace Domain.Interface.Generic
{
    public interface InterfaceGeneric<T> where T: class
    {
        void Add(T Entity);

        void Update(T Entity);

        void Delete(Guid Id);

        List<T> List();

        T Show(Guid Id);
    }
}