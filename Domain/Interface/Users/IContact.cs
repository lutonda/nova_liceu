﻿using Domain.Entity.Users;
using Domain.Interface.Generic;

namespace Domain.Interface.Users
{
    public interface IContact : InterfaceGeneric<Contact>
    {
    }
}
