﻿using Domain.Interface.Generic;

namespace Domain.Interface.Library
{
    public interface IObject : InterfaceGeneric<Entity.Library.Object>
    {
    }
}
