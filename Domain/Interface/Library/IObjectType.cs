﻿using Domain.Entity.Library;
using Domain.Interface.Generic;

namespace Domain.Interface.Library
{
    public interface IObjectType : InterfaceGeneric<ObjectType>
    {
    }
}
