using Domain.Entity.Users;
using Domain.Interface.Generic;

namespace Domain.Interface
{
    public interface IUser : InterfaceGeneric<User>
    {
        
    }
}