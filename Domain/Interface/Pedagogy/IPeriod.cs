using Domain.Entity.Pedagogy;
using Domain.Interface.Generic;

namespace Domain.Interface.Pedagogy
{
    public interface IPeriod : InterfaceGeneric<Period>
    {
        
    }
}