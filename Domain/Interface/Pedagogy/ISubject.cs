using Domain.Entity.Pedagogy;
using Domain.Interface.Generic;

namespace Domain.Interface.Pedagogy
{
    public interface ISubject : InterfaceGeneric<Subject>
    {
        
    }
}