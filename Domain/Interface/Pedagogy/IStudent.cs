using Domain.Entity.Pedagogy;
using Domain.Entity.Users;
using Domain.Interface.Generic;

namespace Domain.Interface.Pedagogy
{
    public interface IStudent : InterfaceGeneric<Student>
    {
        
    }
}