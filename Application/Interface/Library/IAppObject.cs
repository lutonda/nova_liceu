﻿using Domain.Entity.Library;
using Application.Interface.Generic;

namespace Application.Interface.Library
{
    public interface IAppObject : IApplicationGeneric<Object>
    {
    }
}
