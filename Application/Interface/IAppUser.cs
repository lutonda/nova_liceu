using Application.Interface.Generic;
using Domain.Entity.Users;

namespace Application.Interface
{
    public interface IAppUser : IApplicationGeneric<User>
    {
        
    }
}