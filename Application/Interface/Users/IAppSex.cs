using Application.Interface.Generic;
using Domain.Entity.Users;

namespace Application.Interface.Users
{
    public interface IAppSex : IApplicationGeneric<Sex>
    {
        
    }
}