using Application.Interface.Generic;
using Domain.Entity.Users;

namespace Application.Interface.Users
{
    public interface IAppTownship : IApplicationGeneric<Township>
    {
        
    }
}