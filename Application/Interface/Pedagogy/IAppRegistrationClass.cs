using Application.Interface.Generic;
using Domain.Entity.Pedagogy;

namespace Application.Interface.Pedagogy
{
    public interface IAppRegistrationClass : IApplicationGeneric<RegistrationClass>
    {
        
    }
}