﻿using Application.Interface.Generic;
using Domain.Entity.Pedagogy;

namespace Application.Interface.Pedagogy
{
    public interface IAppScore : IApplicationGeneric<Score>
    {
    }
}
