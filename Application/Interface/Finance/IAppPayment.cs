﻿using Application.Interface.Generic;
using Domain.Entity.Finance;

namespace Application.Interface.Finance
{
    public interface IAppPayment : IApplicationGeneric<Payment>
    {
    }
}
