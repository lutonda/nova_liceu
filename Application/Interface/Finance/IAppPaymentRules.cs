﻿using Domain.Entity.Finance;
using Application.Interface.Generic;

namespace Application.Interface.Finance
{
    public interface IAppPaymentRules : IApplicationGeneric<PaymentRules>
    {
    }
}
