using System;
using System.Collections.Generic;

namespace Application.Interface.Generic
{
    public interface IApplicationGeneric<T> where T: class
    {
        void Add(T Entity);

        void Update(T Entity);

        void Delete(Guid Id);

        List<T> List();

        T Show(Guid Id);
    }
}