﻿using System;
using System.Collections.Generic;
using Application.Interface.Pedagogy;
using Domain.Entity.Pedagogy;
using Domain.Interface.Pedagogy;

namespace Application.App.Pedagogy
{
    public class AppTimeTable : IAppTimeTable
    {
        ITimeTable _ITimeTable;

        public AppTimeTable(ITimeTable ITimeTable)
        {
            _ITimeTable = ITimeTable;
        }
        public void Add(TimeTable Entity)
        {
            _ITimeTable.Add(Entity);
        }

        public void Update(TimeTable Entity)
        {
            _ITimeTable.Update(Entity);
        }

        public void Delete(Guid Id)
        {
            _ITimeTable.Delete(Id);
        }

        public List<TimeTable> List()
        {
            return _ITimeTable.List();
        }

        public TimeTable Show(Guid Id)
        {
            return _ITimeTable.Show(Id);
        }
    }
}
