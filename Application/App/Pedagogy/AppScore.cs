﻿using System;
using System.Collections.Generic;
using Application.Interface.Pedagogy;
using Domain.Entity.Pedagogy;
using Domain.Interface.Pedagogy;

namespace Application.App.Pedagogy
{
    public class AppScore : IAppScore
    {
        IScore _IScore;

        public AppScore(IScore IScore)
        {
            _IScore = IScore;
        }
        public void Add(Score Entity)
        {
            _IScore.Add(Entity);
        }

        public void Update(Score Entity)
        {
            _IScore.Update(Entity);
        }

        public void Delete(Guid Id)
        {
            _IScore.Delete(Id);
        }

        public List<Score> List()
        {
            return _IScore.List();
        }

        public Score Show(Guid Id)
        {
            return _IScore.Show(Id);
        }
    }
}
