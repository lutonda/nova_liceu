using System;
using System.Collections.Generic;
using Application.Interface.Pedagogy;
using Domain.Entity.Pedagogy;
using Domain.Interface.Pedagogy;

namespace Application.App.Pedagogy
{
    public class AppStudent : IAppStudent
    {
        IStudent _IStudent;

        public AppStudent(IStudent IStudent)
        {
            _IStudent = IStudent;
        }
        public void Add(Student Entity)
        {
            _IStudent.Add(Entity);
        }

        public void Update(Student Entity)
        {
            _IStudent.Update(Entity);
        }

        public void Delete(Guid Id)
        {
            _IStudent.Delete(Id);
        }

        public List<Student> List()
        {
            return _IStudent.List();
        }

        public Student Show(Guid Id)
        {
           return _IStudent.Show(Id);
        }
    }
}