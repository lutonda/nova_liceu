﻿using System;
using System.Collections.Generic;
using Application.Interface.Pedagogy;
using Domain.Entity.Pedagogy;
using Domain.Interface.Pedagogy;

namespace Application.App.Pedagogy
{
    public class AppStrudentSponsor : IAppStrudentSponsor
    {
        IStrudentSponsor _IStrudentSponsor;

        public AppStrudentSponsor(IStrudentSponsor IStrudentSponsor)
        {
            _IStrudentSponsor = IStrudentSponsor;
        }
        public void Add(StrudentSponsor Entity)
        {
            _IStrudentSponsor.Add(Entity);
        }

        public void Update(StrudentSponsor Entity)
        {
            _IStrudentSponsor.Update(Entity);
        }

        public void Delete(Guid Id)
        {
            _IStrudentSponsor.Delete(Id);
        }

        public List<StrudentSponsor> List()
        {
            return _IStrudentSponsor.List();
        }

        public StrudentSponsor Show(Guid Id)
        {
            return _IStrudentSponsor.Show(Id);
        }
    }
}
