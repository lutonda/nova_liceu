using System;
using System.Collections.Generic;
using Application.Interface.Pedagogy;
using Domain.Entity.Pedagogy;
using Domain.Interface.Pedagogy;

namespace Application.App.Pedagogy
{
    public class AppGrade : IAppGrade
    {
        IGrade _IGrade;

        public AppGrade(IGrade IGrade)
        {
            _IGrade = IGrade;
        }
        public void Add(Grade Entity)
        {
            _IGrade.Add(Entity);
        }

        public void Update(Grade Entity)
        {
            _IGrade.Update(Entity);
        }

        public void Delete(Guid Id)
        {
            _IGrade.Delete(Id);
        }

        public List<Grade> List()
        {
            return _IGrade.List();
        }

        public Grade Show(Guid Id)
        {
           return _IGrade.Show(Id);
        }
    }
}