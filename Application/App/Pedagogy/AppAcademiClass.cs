using System;
using System.Collections.Generic;
using Application.Interface.Pedagogy;
using Domain.Entity.Pedagogy;
using Domain.Interface.Pedagogy;

namespace Application.App.Pedagogy
{
    public class AppAcademiClass : IAppAcademiClass
    {
        IAcademiClass _IAcademiClass;

        public AppAcademiClass(IAcademiClass IAcademiClass)
        {
            _IAcademiClass = IAcademiClass;
        }
        public void Add(AcademiClass Entity)
        {
            _IAcademiClass.Add(Entity);
        }

        public void Update(AcademiClass Entity)
        {
            _IAcademiClass.Update(Entity);
        }

        public void Delete(Guid Id)
        {
            _IAcademiClass.Delete(Id);
        }

        public List<AcademiClass> List()
        {
            return _IAcademiClass.List();
        }

        public AcademiClass Show(Guid Id)
        {
           return _IAcademiClass.Show(Id);
        }
    }
}