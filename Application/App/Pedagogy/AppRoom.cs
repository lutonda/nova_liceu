using System;
using System.Collections.Generic;
using Application.Interface.Pedagogy;
using Domain.Entity.Pedagogy;
using Domain.Interface.Pedagogy;

namespace Application.App.Pedagogy
{
    public class AppRoom : IAppRoom
    {
        IRoom _IRoom;

        public AppRoom(IRoom IRoom)
        {
            _IRoom = IRoom;
        }
        public void Add(Room Entity)
        {
            _IRoom.Add(Entity);
        }

        public void Update(Room Entity)
        {
            _IRoom.Update(Entity);
        }

        public void Delete(Guid Id)
        {
            _IRoom.Delete(Id);
        }

        public List<Room> List()
        {
            return _IRoom.List();
        }

        public Room Show(Guid Id)
        {
           return _IRoom.Show(Id);
        }
    }
}