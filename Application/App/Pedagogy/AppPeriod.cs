using System;
using System.Collections.Generic;
using Application.Interface.Pedagogy;
using Domain.Entity.Pedagogy;
using Domain.Interface.Pedagogy;

namespace Application.App.Pedagogy
{
    public class AppPeriod : IAppPeriod
    {
        IPeriod _IPeriod;

        public AppPeriod(IPeriod IPeriod)
        {
            _IPeriod = IPeriod;
        }
        public void Add(Period Entity)
        {
            _IPeriod.Add(Entity);
        }

        public void Update(Period Entity)
        {
            _IPeriod.Update(Entity);
        }

        public void Delete(Guid Id)
        {
            _IPeriod.Delete(Id);
        }

        public List<Period> List()
        {
            return _IPeriod.List();
        }

        public Period Show(Guid Id)
        {
           return _IPeriod.Show(Id);
        }
    }
}