using System;
using System.Collections.Generic;
using Application.Interface.Pedagogy;
using Domain.Entity.Pedagogy;
using Domain.Interface.Pedagogy;

namespace Application.App.Pedagogy
{
    public class AppProfessor : IAppProfessor
    {
        IProfessor _IProfessor;

        public AppProfessor(IProfessor IProfessor)
        {
            _IProfessor = IProfessor;
        }
        public void Add(Professor Entity)
        {
            _IProfessor.Add(Entity);
        }

        public void Update(Professor Entity)
        {
            _IProfessor.Update(Entity);
        }

        public void Delete(Guid Id)
        {
            _IProfessor.Delete(Id);
        }

        public List<Professor> List()
        {
            return _IProfessor.List();
        }

        public Professor Show(Guid Id)
        {
           return _IProfessor.Show(Id);
        }
    }
}