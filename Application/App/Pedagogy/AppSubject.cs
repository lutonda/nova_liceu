using System;
using System.Collections.Generic;
using Application.Interface.Pedagogy;
using Domain.Entity.Pedagogy;
using Domain.Interface.Pedagogy;

namespace Application.App.Pedagogy
{
    public class AppSubject : IAppSubject
    {
        ISubject _ISubject;

        public AppSubject(ISubject ISubject)
        {
            _ISubject = ISubject;
        }
        public void Add(Subject Entity)
        {
            _ISubject.Add(Entity);
        }

        public void Update(Subject Entity)
        {
            _ISubject.Update(Entity);
        }

        public void Delete(Guid Id)
        {
            _ISubject.Delete(Id);
        }

        public List<Subject> List()
        {
            return _ISubject.List();
        }

        public Subject Show(Guid Id)
        {
           return _ISubject.Show(Id);
        }
    }
}