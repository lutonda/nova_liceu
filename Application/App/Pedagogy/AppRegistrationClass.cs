using System;
using System.Collections.Generic;
using Application.Interface.Pedagogy;
using Domain.Entity.Pedagogy;
using Domain.Interface.Pedagogy;

namespace Application.App.Pedagogy
{
    public class AppRegistrationClass : IAppRegistrationClass
    {
        IRegistrationClass _IRegistrationClass;

        public AppRegistrationClass(IRegistrationClass IRegistrationClass)
        {
            _IRegistrationClass = IRegistrationClass;
        }
        public void Add(RegistrationClass Entity)
        {
            _IRegistrationClass.Add(Entity);
        }

        public void Update(RegistrationClass Entity)
        {
            _IRegistrationClass.Update(Entity);
        }

        public void Delete(Guid Id)
        {
            _IRegistrationClass.Delete(Id);
        }

        public List<RegistrationClass> List()
        {
            return _IRegistrationClass.List();
        }

        public RegistrationClass Show(Guid Id)
        {
           return _IRegistrationClass.Show(Id);
        }
    }
}