﻿using System;
using System.Collections.Generic;
using Application.Interface.Pedagogy;
using Domain.Entity.Pedagogy;
using Domain.Interface.Pedagogy;

namespace Application.App.Pedagogy
{
    public class AppAlocatedSubject : IAppAlocatedSubject
    {
        IAlocatedSubject _IAlocatedSubject;

        public AppAlocatedSubject(IAlocatedSubject IAlocatedSubject)
        {
            _IAlocatedSubject = IAlocatedSubject;
        }
        public void Add(AlocatedSubject Entity)
        {
            _IAlocatedSubject.Add(Entity);
        }

        public void Update(AlocatedSubject Entity)
        {
            _IAlocatedSubject.Update(Entity);
        }

        public void Delete(Guid Id)
        {
            _IAlocatedSubject.Delete(Id);
        }

        public List<AlocatedSubject> List()
        {
            return _IAlocatedSubject.List();
        }

        public AlocatedSubject Show(Guid Id)
        {
            return _IAlocatedSubject.Show(Id);
        }
    }
}
