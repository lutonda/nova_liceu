﻿using System;
using System.Collections.Generic;
using Application.Interface.Pedagogy;
using Domain.Entity.Pedagogy;
using Domain.Interface.Pedagogy;

namespace Application.App.Pedagogy
{
    public class AppTimeSheet : IAppTimeSheet
    {
        ITimeSheet _ITimeSheet;

        public AppTimeSheet(ITimeSheet ITimeSheet)
        {
            _ITimeSheet = ITimeSheet;
        }
        public void Add(TimeSheet Entity)
        {
            _ITimeSheet.Add(Entity);
        }

        public void Update(TimeSheet Entity)
        {
            _ITimeSheet.Update(Entity);
        }

        public void Delete(Guid Id)
        {
            _ITimeSheet.Delete(Id);
        }

        public List<TimeSheet> List()
        {
            return _ITimeSheet.List();
        }

        public TimeSheet Show(Guid Id)
        {
            return _ITimeSheet.Show(Id);
        }
    }
}
