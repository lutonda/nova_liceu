﻿using System;
using System.Collections.Generic;
using Application.Interface.Users;
using Domain.Entity.Users;
using Domain.Interface.Users;

namespace Application.App.Users
{
    public class AppContact : IAppContact
    {
        IContact _IContact;

        public AppContact(IContact IContact)
        {
            _IContact = IContact;
        }
        public void Add(Contact Entity)
        {
            _IContact.Add(Entity);
        }

        public void Update(Contact Entity)
        {
            _IContact.Update(Entity);
        }

        public void Delete(Guid Id)
        {
            _IContact.Delete(Id);
        }

        public List<Contact> List()
        {
            return _IContact.List();
        }

        public Contact Show(Guid Id)
        {
            return _IContact.Show(Id);
        }
    }
}
