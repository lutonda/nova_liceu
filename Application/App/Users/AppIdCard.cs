﻿using System;
using System.Collections.Generic;
using Application.Interface.Users;
using Domain.Entity.Users;
using Domain.Interface.Users;

namespace Application.App.Users
{
    public class AppIdCard : IAppIdCard
    {
        IIdCard _IIdCard;

        public AppIdCard(IIdCard IIdCard)
        {
            _IIdCard = IIdCard;
        }
        public void Add(IdCard Entity)
        {
            _IIdCard.Add(Entity);
        }

        public void Update(IdCard Entity)
        {
            _IIdCard.Update(Entity);
        }

        public void Delete(Guid Id)
        {
            _IIdCard.Delete(Id);
        }

        public List<IdCard> List()
        {
            return _IIdCard.List();
        }

        public IdCard Show(Guid Id)
        {
            return _IIdCard.Show(Id);
        }
    }
}
