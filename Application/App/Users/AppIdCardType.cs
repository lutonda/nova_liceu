using System;
using System.Collections.Generic;
using Application.Interface.Users;
using Domain.Entity.Users;
using Domain.Interface.Users;

namespace Application.App.Users
{
    public class AppIdCardType : IAppIdCardType
    {
        IIdCardType _IIdCardType;

        public AppIdCardType(IIdCardType IIdCardType)
        {
            _IIdCardType = IIdCardType;
        }
        public void Add(IdCardType Entity)
        {
            _IIdCardType.Add(Entity);
        }

        public void Update(IdCardType Entity)
        {
            _IIdCardType.Update(Entity);
        }

        public void Delete(Guid Id)
        {
            _IIdCardType.Delete(Id);
        }

        public List<IdCardType> List()
        {
            return _IIdCardType.List();
        }

        public IdCardType Show(Guid Id)
        {
           return _IIdCardType.Show(Id);
        }
    }
}