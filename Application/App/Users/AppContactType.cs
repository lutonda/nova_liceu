using System;
using System.Collections.Generic;
using Application.Interface.Users;
using Domain.Entity.Users;
using Domain.Interface.Users;

namespace Application.App.Users
{
    public class AppContactType : IAppContactType
    {
        IContactType _IContactType;

        public AppContactType(IContactType IContactType)
        {
            _IContactType = IContactType;
        }
        public void Add(ContactType Entity)
        {
            _IContactType.Add(Entity);
        }

        public void Update(ContactType Entity)
        {
            _IContactType.Update(Entity);
        }

        public void Delete(Guid Id)
        {
            _IContactType.Delete(Id);
        }

        public List<ContactType> List()
        {
            return _IContactType.List();
        }

        public ContactType Show(Guid Id)
        {
           return _IContactType.Show(Id);
        }
    }
}