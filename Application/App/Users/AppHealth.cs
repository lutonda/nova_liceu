﻿using System;
using System.Collections.Generic;
using Application.Interface.Users;
using Domain.Entity.Users;
using Domain.Interface.Users;

namespace Application.App.Users
{
    public class AppHealth : IAppHealth
    {
        IHealth _IHealth;

        public AppHealth(IHealth IHealth)
        {
            _IHealth = IHealth;
        }
        public void Add(Health Entity)
        {
            _IHealth.Add(Entity);
        }

        public void Update(Health Entity)
        {
            _IHealth.Update(Entity);
        }

        public void Delete(Guid Id)
        {
            _IHealth.Delete(Id);
        }

        public List<Health> List()
        {
            return _IHealth.List();
        }

        public Health Show(Guid Id)
        {
            return _IHealth.Show(Id);
        }
    }
}
