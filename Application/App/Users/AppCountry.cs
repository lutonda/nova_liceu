﻿using System;
using System.Collections.Generic;
using Application.Interface.Users;
using Domain.Entity.Users;
using Domain.Interface.Users;

namespace Application.App.Users
{
    public class AppCountry : IAppCountry
    {
        ICountry _ICountry;

        public AppCountry(ICountry ICountry)
        {
            _ICountry = ICountry;
        }
        public void Add(Country Entity)
        {
            _ICountry.Add(Entity);
        }

        public void Update(Country Entity)
        {
            _ICountry.Update(Entity);
        }

        public void Delete(Guid Id)
        {
            _ICountry.Delete(Id);
        }

        public List<Country> List()
        {
            return _ICountry.List();
        }

        public Country Show(Guid Id)
        {
            return _ICountry.Show(Id);
        }
    }
}
