﻿using System;
using System.Collections.Generic;
using Application.Interface.Users;
using Domain.Entity.Users;
using Domain.Interface.Users;


namespace Application.App.Users
{
    public class AppAddress : IAppAddress
    {
        IAddress _IAddress;

        public AppAddress(IAddress IAddress)
        {
            _IAddress = IAddress;
        }
        public void Add(Address Entity)
        {
            _IAddress.Add(Entity);
        }

        public void Update(Address Entity)
        {
            _IAddress.Update(Entity);
        }

        public void Delete(Guid Id)
        {
            _IAddress.Delete(Id);
        }

        public List<Address> List()
        {
            return _IAddress.List();
        }

        public Address Show(Guid Id)
        {
            return _IAddress.Show(Id);
        }
    }
}
