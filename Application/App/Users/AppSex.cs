using System;
using System.Collections.Generic;
using Application.Interface.Users;
using Domain.Entity.Users;
using Domain.Interface.Users;

namespace Application.App.Users
{
    public class AppSex : IAppSex
    {
        ISex _ISex;

        public AppSex(ISex ISex)
        {
            _ISex = ISex;
        }
        public void Add(Sex Entity)
        {
            _ISex.Add(Entity);
        }

        public void Update(Sex Entity)
        {
            _ISex.Update(Entity);
        }

        public void Delete(Guid Id)
        {
            _ISex.Delete(Id);
        }

        public List<Sex> List()
        {
            return _ISex.List();
        }

        public Sex Show(Guid Id)
        {
           return _ISex.Show(Id);
        }
    }
}