using System;
using System.Collections.Generic;
using Application.Interface.Users;
using Domain.Entity.Users;
using Domain.Interface.Users;

namespace Application.App.Users
{
    public class AppPerson : IAppPerson
    {
        IPerson _IPerson;

        public AppPerson(IPerson IPerson)
        {
            _IPerson = IPerson;
        }
        public void Add(Person Entity)
        {
            _IPerson.Add(Entity);
        }

        public void Update(Person Entity)
        {
            _IPerson.Update(Entity);
        }

        public void Delete(Guid Id)
        {
            _IPerson.Delete(Id);
        }

        public List<Person> List()
        {
            return _IPerson.List();
        }

        public Person Show(Guid Id)
        {
           return _IPerson.Show(Id);
        }
    }
}