using System;
using System.Collections.Generic;
using Application.Interface.Users;
using Domain.Entity.Users;
using Domain.Interface.Users;

namespace Application.App.Users
{
    public class AppProvince : IAppProvince
    {
        IProvince _IProvince;

        public AppProvince(IProvince IProvince)
        {
            _IProvince = IProvince;
        }
        public void Add(Province Entity)
        {
            _IProvince.Add(Entity);
        }

        public void Update(Province Entity)
        {
            _IProvince.Update(Entity);
        }

        public void Delete(Guid Id)
        {
            _IProvince.Delete(Id);
        }

        public List<Province> List()
        {
            return _IProvince.List();
        }

        public Province Show(Guid Id)
        {
           return _IProvince.Show(Id);
        }
    }
}