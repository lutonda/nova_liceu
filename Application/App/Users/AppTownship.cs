using System;
using System.Collections.Generic;
using Application.Interface.Users;
using Domain.Entity.Users;
using Domain.Interface.Users;

namespace Application.App.Users
{
    public class AppTownship : IAppTownship
    {
        ITownship _ITownship;

        public AppTownship(ITownship ITownship)
        {
            _ITownship = ITownship;
        }
        public void Add(Township Entity)
        {
            _ITownship.Add(Entity);
        }

        public void Update(Township Entity)
        {
            _ITownship.Update(Entity);
        }

        public void Delete(Guid Id)
        {
            _ITownship.Delete(Id);
        }

        public List<Township> List()
        {
            return _ITownship.List();
        }

        public Township Show(Guid Id)
        {
           return _ITownship.Show(Id);
        }
    }
}