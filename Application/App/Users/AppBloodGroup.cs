﻿using System;
using System.Collections.Generic;
using Application.Interface.Users;
using Domain.Entity.Users;
using Domain.Interface.Users;

namespace Application.App.Users
{
    public class AppBloodGroup : IAppBloodGroup
    {
        IBloodGroup _IBloodGroup;

        public AppBloodGroup(IBloodGroup IBloodGroup)
        {
            _IBloodGroup = IBloodGroup;
        }
        public void Add(BloodGroup Entity)
        {
            _IBloodGroup.Add(Entity);
        }

        public void Update(BloodGroup Entity)
        {
            _IBloodGroup.Update(Entity);
        }

        public void Delete(Guid Id)
        {
            _IBloodGroup.Delete(Id);
        }

        public List<BloodGroup> List()
        {
            return _IBloodGroup.List();
        }

        public BloodGroup Show(Guid Id)
        {
            return _IBloodGroup.Show(Id);
        }
    }
}
