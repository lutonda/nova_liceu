﻿using Application.Interface.Finance;
using Domain.Entity.Finance;
using Domain.Interface.Finance;
using System;
using System.Collections.Generic;

namespace Application.App.Finance
{
    public class AppPaymentSlip : IAppPaymentSlip
    {
        IPaymentSlip _IPaymentSlip;
        public AppPaymentSlip(IPaymentSlip IPaymentSlip)
        {
            _IPaymentSlip = IPaymentSlip;
        }

        public void Add(PaymentSlip Entity)
        {
            _IPaymentSlip.Add(Entity);
        }

        public void Update(PaymentSlip Entity)
        {
            _IPaymentSlip.Update(Entity);
        }

        public void Delete(Guid Id)
        {
            _IPaymentSlip.Delete(Id);
        }

        public List<PaymentSlip> List()
        {
            return _IPaymentSlip.List();
        }

        public PaymentSlip Show(Guid Id)
        {
            return _IPaymentSlip.Show(Id);
        }
    }
}
