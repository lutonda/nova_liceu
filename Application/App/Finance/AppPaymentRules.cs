﻿using Application.Interface.Finance;
using Domain.Entity.Finance;
using Domain.Interface.Finance;
using System;
using System.Collections.Generic;

namespace Application.App.Finance
{
    public class AppPaymentRules : IAppPaymentRules
    {
        IPaymentRules _IPaymentRules;
        public AppPaymentRules(IPaymentRules IPaymentRules)
        {
            _IPaymentRules = IPaymentRules;
        }

        public void Add(PaymentRules Entity)
        {
            _IPaymentRules.Add(Entity);
        }

        public void Update(PaymentRules Entity)
        {
            _IPaymentRules.Update(Entity);
        }

        public void Delete(Guid Id)
        {
            _IPaymentRules.Delete(Id);
        }

        public List<PaymentRules> List()
        {
            return _IPaymentRules.List();
        }

        public PaymentRules Show(Guid Id)
        {
            return _IPaymentRules.Show(Id);
        }
    }
}
