﻿using Application.Interface.Finance;
using Domain.Entity.Finance;
using Domain.Interface.Finance;
using System;
using System.Collections.Generic;

namespace Application.App.Finance
{
    public class AppPayment : IAppPayment
    {
        IPayment _IPayment;
        public AppPayment(IPayment IPayment)
        {
            _IPayment = IPayment;
        }
        
        public void Add(Payment Entity)
        {
            _IPayment.Add(Entity);
        }

        public void Update(Payment Entity)
        {
            _IPayment.Update(Entity);
        }

        public void Delete(Guid Id)
        {
            _IPayment.Delete(Id);
        }

        public List<Payment> List()
        {
            return _IPayment.List();
        }

        public Payment Show(Guid Id)
        {
            return _IPayment.Show(Id);
        }
    }
}
