﻿using Application.Interface.Library;
using Domain.Entity.Library;
using System;
using System.Collections.Generic;

namespace Application.App.Library
{
    public class AppObjectType : IAppObjectType
    {
        IAppObjectType _IAppObjectType;
        public AppObjectType(IAppObjectType IAppObjectType)
        {
            _IAppObjectType = IAppObjectType;
        }

        public void Add(ObjectType Entity)
        {
            _IAppObjectType.Add(Entity);
        }

        public void Update(ObjectType Entity)
        {
            _IAppObjectType.Update(Entity);
        }

        public void Delete(Guid Id)
        {
            _IAppObjectType.Delete(Id);
        }

        public List<ObjectType> List()
        {
            return _IAppObjectType.List();
        }

        public ObjectType Show(Guid Id)
        {
            return _IAppObjectType.Show(Id);
        }
    }
}
