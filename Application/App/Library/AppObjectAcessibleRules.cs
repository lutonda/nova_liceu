﻿using Application.Interface.Library;
using System;
using System.Collections.Generic;
using Domain.Entity.Library;

namespace Application.App.Library
{
    class AppObjectAcessibleRules : IAppObjectAccessibleRules
    {
        IAppObjectAccessibleRules _IAppObjectAcessibleRules;
        public AppObjectAcessibleRules(IAppObjectAccessibleRules IAppObjectAcessibleRules)
        {
            _IAppObjectAcessibleRules = IAppObjectAcessibleRules;
        }

        public void Add(ObjectAccessibleRules Entity)
        {
            _IAppObjectAcessibleRules.Add(Entity);
        }

        public void Update(ObjectAccessibleRules Entity)
        {
            _IAppObjectAcessibleRules.Update(Entity);
        }

        public void Delete(Guid Id)
        {
            _IAppObjectAcessibleRules.Delete(Id);
        }

        public List<ObjectAccessibleRules> List()
        {
            return _IAppObjectAcessibleRules.List();
        }

        public ObjectAccessibleRules Show(System.Guid Id)
        {
            return _IAppObjectAcessibleRules.Show(Id);
        }
    }
}
