﻿using Application.Interface.Library;
using Domain.Entity.Library;
using System.Collections.Generic;

namespace Application.App.Library
{
    public class AppObject : IAppObject
    {
        IAppObject _IAppObject;
        public AppObject(IAppObject IAppObject)
        {
            _IAppObject = IAppObject;
        }

        public void Add(Object Entity)
        {
            _IAppObject.Add(Entity);
        }

        public void Update(Object Entity)
        {
            _IAppObject.Update(Entity);
        }

        public void Delete(System.Guid Id)
        {
            _IAppObject.Delete(Id);
        }

        public List<Object> List()
        {
            return _IAppObject.List();
        }

        public Object Show(System.Guid Id)
        {
            return _IAppObject.Show(Id);
        }
    }
}
