using System;
using System.Collections.Generic;
using Application.Interface;
using Domain.Entity.Users;
using Domain.Interface;

namespace Application.App
{
    public class AppUser : IAppUser
    {
        IUser _IUser;

        public AppUser(IUser IUser)
        {
            _IUser = IUser;
        }
        public void Add(User Entity)
        {
            _IUser.Add(Entity);
        }

        public void Update(User Entity)
        {
            _IUser.Update(Entity);
        }

        public void Delete(Guid Id)
        {
            _IUser.Delete(Id);
        }

        public List<User> List()
        {
            return _IUser.List();
        }

        public User Show(Guid Id)
        {
           return _IUser.Show(Id);
        }
    }
}